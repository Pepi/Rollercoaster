function [out1 out2] = GenerateObjFile(curve, roll, scale)
    
    len = length(curve);
    tanCurve = zeros(len, 3);
    upCurve = zeros(len, 3);
    latCurve = zeros(len,3);
    % Sweep all points of the curve
    for i = 1:len
        if i > 1
            p0 = curve(i-1,:);
        else
            p0 = curve(len,:);
        end
        if i < len
            p2 = curve(i+1,:);
        else
            p2 = curve(1,:);
        end
        
        % Tangeant Generation        
        vtan = zeros(1,3);
        vup = zeros(1,3);
        vlat = zeros(1,3);
        for j = 1:3
            vtan(j) = p2(j) - p0(j);
        end
        vtan = vtan/norm(vtan);
        
        
        % Up Vector Generation
        vz = [0.0 0.0 1.0];
        nvz = norm(vtan)/(sum(vz.*vtan)/(norm(vz)*norm(vtan)));
        vz = vz * abs(nvz);
        if vtan(3) < 0.0
            vup = vz + vtan;
        else
            vup = vz - vtan;
        end
        vup = vup / norm(vup);
        vup = rotationMatrix(vtan,roll(i))*vup';
        
        % Lat Vector Generation
        vlat = [(vtan(2)*vup(3) - vtan(3)*vup(2)) (vtan(3)*vup(1) - vtan(1)*vup(3)) (vtan(1)*vup(2) - vtan(2)*vup(1))];

        % Add computed vectors to curve
        tanCurve(i,:) = vtan;
        upCurve(i,:) = vup;
        latCurve(i,:) = vlat;
    end

% Draw Tangeants
%     draws = zeros(2,3);
%     for i = 1:len
%         draws(1,:) = curve(i,:);
%         draws(2,:) = curve(i,:) + tanCurve(i,:);
%         plotCurve(draws);
%     end
% Draw UpVector
%     draws = zeros(2,3);
%     for i = 1:len
%         draws(1,:) = curve(i,:);
%         draws(2,:) = curve(i,:) + upCurve(i,:);
%         plotCurve(draws);
%     end
% Draw LatVector
    draws = zeros(2,3);
    for i = 1:len
        draws(1,:) = curve(i,:);
        draws(2,:) = curve(i,:) + 5*latCurve(i,:);
        plotCurve(draws);
    end

    space = 1.3;
    rodrad= 0.2;
    
    nseg = 20;
    vLen = len*2*nseg;
    iLen = len*4*nseg;
    vertices = zeros(len*2*nseg,3);
    verticesIndex = zeros(len*4*nseg,3);
    
    for i = 0:len-1
        % Compute the two circle at each side of the directive line
        % First get the centers
        p1 = curve(i+1,:) + latCurve(i+1,:)*space/2;
        p2 = curve(i+1,:) - latCurve(i+1,:)*space/2;
        % And then the circles from those centers
        circle1 = circlePoints(p1, rodrad, tanCurve(i+1,:), nseg);
        circle2 = circlePoints(p2, rodrad, tanCurve(i+1,:), nseg);
        %plotCurve(circle1);
        %plotCurve(circle2);
              
        for j = 0:nseg-1
            verticesIndex(4*nseg*i + 4*j+1,1) = mod(i*2*nseg + mod(j+1, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+1,2) = mod((i+1)*2*nseg + mod(j, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+1,3) = mod(i*2*nseg + mod(j, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+2,1) = mod((i+1)*2*nseg + mod(j+1, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+2,2) = mod((i+1)*2*nseg + mod(j, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+2,3) = mod(i*2*nseg + mod(j+1, nseg),vLen)+1;
            verticesIndex(4*nseg*i + 4*j+3,1) = mod(i*2*nseg + mod(j+1, nseg)+nseg,vLen)+1;
            verticesIndex(4*nseg*i + 4*j+3,2) = mod((i+1)*2*nseg + mod(j, nseg)+nseg,vLen)+1;
            verticesIndex(4*nseg*i + 4*j+3,3) = mod(i*2*nseg + mod(j, nseg)+nseg,vLen)+1;
            verticesIndex(4*nseg*i + 4*j+4,1) = mod((i+1)*2*nseg + mod(j+1, nseg)+nseg,vLen)+1;
            verticesIndex(4*nseg*i + 4*j+4,2) = mod((i+1)*2*nseg + mod(j, nseg)+nseg,vLen)+1;
            verticesIndex(4*nseg*i + 4*j+4,3) = mod(i*2*nseg + mod(j+1, nseg)+nseg,vLen)+1;
            vertices(2*i*nseg+j+1,:) = circle1(j+1,:);
            vertices(2*i*nseg+j+nseg+1,:) = circle2(j+1,:);
        end
    end

    out1 = vertices;
    out2 = verticesIndex;
    
    file = fopen('Rollercoaster.obj', 'w+');
    fprintf(file, '# OBJ\n');
    
    for i = 1:vLen
        fprintf(file, 'v %f %f %f\n', vertices(i,1)*scale, vertices(i,3)*scale, vertices(i,2)*scale);
    end
    
    for i = 1:iLen
        fprintf(file, 'f %d %d %d\n', verticesIndex(i,1), verticesIndex(i,2), verticesIndex(i,3));
    end
    fclose(file);
end


function out = circlePoints(center, radius, axis, nseg)
    circle = zeros(nseg,3);
    for i = 1:nseg
        ang = 2*pi/(nseg+1)*i;
        circle(i,:) = center' + rotationMatrix(axis, ang) * (radius*[0 0 1])';
    end
    out = circle;
end

function out = rotationMatrix(axe, angle)
    R = zeros(3);
    R(1,1) = cos(angle) + (axe(1)^2)*(1-cos(angle));
    R(1,2) = axe(1)*axe(2)*(1-cos(angle)) - axe(3)*sin(angle);
    R(1,3) = axe(1)*axe(3)*(1-cos(angle)) + axe(2)*sin(angle);
    R(2,1) = axe(2)*axe(1)*(1-cos(angle)) + axe(3)*sin(angle);
    R(2,2) = cos(angle) + (axe(2)^2)*(1-cos(angle));
    R(2,3) = axe(2)*axe(3)*(1-cos(angle)) - axe(1)*sin(angle);
    R(3,1) = axe(3)*axe(1)*(1-cos(angle)) - axe(2)*sin(angle);
    R(3,2) = axe(3)*axe(2)*(1-cos(angle)) + axe(1)*sin(angle);
    R(3,3) = cos(angle) + (axe(3)^2)*(1-cos(angle));
    out = R;
end
