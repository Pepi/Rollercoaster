    function out = interpolateCurve(curve, method, mu, tension, bias)
	[len, dim] = size(curve);
	newCurve = zeros(2*len,3);
	p3 = curve(1,:);
	p2 = curve(len,:);
	p1 = curve(len-1,:);
	p0 = curve(len-2,:);
	for i = 1:len
		p3 = curve(i,:);
		newCurve(2*(i-1) + 1,:) = p1;
		newPoint = zeros(1,dim);
		for j = 1:dim
            if strcmp(method,'cubic')
                newPoint(j) = CubicInterpolate(p0(j), p1(j), p2(j), p3(j), mu);
            elseif strcmp(method,'hermite')
                newPoint(j) = HermiteInterpolate(p0(j), p1(j), p2(j), p3(j), mu, tension, bias);
            end
                
        end
		newCurve(2*(i-1) + 2,:) = newPoint;
		p0 = p1;
		p1 = p2;
		p2 = p3;
    end
	out = newCurve;
end

% Catmull-Rom splines
function out = CubicInterpolate(y0, y1, y2, y3, mu)
	mu2 = mu*mu;
	a0 = -0.5*y0 + 1.5*y1 - 1.5*y2 + 0.5*y3;
    a1 = y0 - 2.5*y1 + 2*y2 - 0.5*y3;
    a2 = -0.5*y0 + 0.5*y2;
    a3 = y1;

	out = (a0*mu*mu2+a1*mu2+a2*mu+a3);
end

function out = HermiteInterpolate(y0, y1, y2, y3, mu, tension, bias)
    mu2 = mu * mu;
    mu3 = mu2 * mu;
    m0 = (y1-y0)*(1+bias)*(1-tension)/2;
    m0 = m0 + (y2-y1)*(1-bias)*(1-tension)/2;
    m1 = (y2-y1)*(1+bias)*(1-tension)/2;
    m1 = m1 + (y3-y2)*(1-bias)*(1-tension)/2;
    a0 = 2*mu3 - 3*mu2 + 1;
    a1 = mu3 - 2*mu2 + mu;
    a2 = mu3 -   mu2;
    a3 = -2*mu3 + 3*mu2;

    out = (a0*y1+a1*m0+a2*m1+a3*y2);
end
