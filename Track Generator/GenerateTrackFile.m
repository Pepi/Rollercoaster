function [out1 out2] = GenerateTrackFile(curve, roll, accel, scale)
    
    len = length(curve);
    tanCurve = zeros(len, 3);
    upCurve = zeros(len, 3);
    latCurve = zeros(len,3);        
    vtan = zeros(1,3);
    vup = zeros(1,3);
    vlat = zeros(1,3);
    % Sweep all points of the curve
    for i = 1:len
        if i > 1
            p0 = curve(i-1,:);
        else
            p0 = curve(len,:);
        end
        if i < len
            p2 = curve(i+1,:);
        else
            p2 = curve(1,:);
        end
        
        % Tangeant Generation    
        for j = 1:3
            vtan(j) = p2(j) - p0(j);
        end
        vtan = vtan/norm(vtan);
        
        
        % Up Vector Generation
        vz = [0.0 0.0 1.0];
        nvz = norm(vtan)/(sum(vz.*vtan)/(norm(vz)*norm(vtan)));
        vz = vz * abs(nvz);
        if vtan(3) < 0.0
            vup = vz + vtan;
        else
            vup = vz - vtan;
        end
        vup = vup / norm(vup);
        
        % Lat Vector Generation
        vlat = [(vtan(2)*vup(3) - vtan(3)*vup(2)) (vtan(3)*vup(1) - vtan(1)*vup(3)) (vtan(1)*vup(2) - vtan(2)*vup(1))];
        vlat = rotationMatrix(vtan,roll(i))*vlat';

        % Add computed vectors to curve
        tanCurve(i,:) = vtan;
        upCurve(i,:) = vup;
        latCurve(i,:) = vlat;
    end

% Draw Tangeants
%     draws = zeros(2,3);
%     for i = 1:len
%         draws(1,:) = curve(i,:);
%         draws(2,:) = curve(i,:) + tangeantCurve(i,:);
%         plotCurve(draws);
%     end
% Draw UpVector
%     draws = zeros(2,3);
%     for i = 1:len
%         draws(1,:) = curve(i,:);
%         draws(2,:) = curve(i,:) + upCurve(i,:);
%         plotCurve(draws);
%     end
% Draw LatVector
%     draws = zeros(2,3);
%     for i = 1:len
%         draws(1,:) = curve(i,:);
%         draws(2,:) = curve(i,:) + latCurve(i,:);
%         plotCurve(draws);
%     end
    
    file = fopen('Rollercoaster.trac', 'w+');
    
    for i = 1:len
        fprintf(file, 'p %f %f %f\n', curve(i,1)*scale, curve(i,3)*scale, curve(i,2)*scale);
        fprintf(file, 't %f %f %f\n', tanCurve(i,1)*scale, tanCurve(i,3)*scale, tanCurve(i,2)*scale);
        fprintf(file, 'u %f %f %f\n', upCurve(i,1)*scale, upCurve(i,3)*scale, upCurve(i,2)*scale);
        fprintf(file, 'b %f %f %f\n', latCurve(i,1)*scale, latCurve(i,3)*scale, latCurve(i,2)*scale);
        fprintf(file, 'a %f\n', accel(i));
    end
    fclose(file);
end

function out = rotationMatrix(axe, angle)
    R = zeros(3);
    R(1,1) = cos(angle) + (axe(1)^2)*(1-cos(angle));
    R(1,2) = axe(1)*axe(2)*(1-cos(angle)) - axe(3)*sin(angle);
    R(1,3) = axe(1)*axe(3)*(1-cos(angle)) + axe(2)*sin(angle);
    R(2,1) = axe(2)*axe(1)*(1-cos(angle)) + axe(3)*sin(angle);
    R(2,2) = cos(angle) + (axe(2)^2)*(1-cos(angle));
    R(2,3) = axe(2)*axe(3)*(1-cos(angle)) - axe(1)*sin(angle);
    R(3,1) = axe(3)*axe(1)*(1-cos(angle)) - axe(2)*sin(angle);
    R(3,2) = axe(3)*axe(2)*(1-cos(angle)) + axe(1)*sin(angle);
    R(3,3) = cos(angle) + (axe(3)^2)*(1-cos(angle));
    out = R;
end
