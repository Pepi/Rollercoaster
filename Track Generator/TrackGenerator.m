clc;clear;close all;

% mu set to 1/2 to have equal balance between points parameters
mu = 0.5;
scale = 40.0;
vTrac = 0.5;

points = scale * [[0.0,0.0,0.15];[0.0,1.0,0.3];[1.0,2.0,0.1];[1.8,1.5,0.25];[2.0,0.75,0.1];[2.0,0.0,0.85];[1.85,-0.5,0.0];[1.5,-1.5,0.4];[1.0,-2.0,0.25];[-0.25,-1.5,0.15]];
roll = pi/180*[290.0;40.0;30.0;25.0;10.0;0.0;15.0;80.0;30.0;32.0];
accel = [0.0; 0.0; 0.0; 0.0; vTrac; vTrac; 0.0; 0.0; 0.0;0.0];

%plotCurve(points);

for i = 0:4
    points = interpolateCurve(points, 'hermite',mu, 0.03, 0.3);
    roll = interpolateCurve(roll, 'hermite',mu, 0.2, 0.1);
    accel = interpolateCurve(accel, 'hermite',mu, 0.9, 0.9);
end

len = length(points);
plotCurve(points);

ratio = 0.5;

GenerateObjfile(points, roll, ratio);

for i = 0:3
    points = interpolateCurve(points, 'hermite',mu, 0.03, 0.3);
    roll = interpolateCurve(roll, 'hermite',mu, 0.2, 0.1);
    accel = interpolateCurve(accel, 'hermite',mu, 0.9, 0.9);
end

for i = 1:length(accel)
    a = accel(i);
    if a > vTrac/2
        accel(i) = vTrac;
    else
        accel(i) = 0;
    end
end

GenerateTrackFile(points, roll, accel, ratio);