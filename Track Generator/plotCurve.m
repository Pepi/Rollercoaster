function plotCurve(points)
    [len dim] = size(points);
    X = zeros(1,len);
    Y = zeros(1,len);
    Z = zeros(1,len);
    for i = 1:len
        X(i) = points(i,1);
        Y(i) = points(i,2);
        Z(i) = points(i,3);
    end

    plot3(X,Y,Z);
    h = get(gca,'DataAspectRatio');
    if h(3)==1
          set(gca,'DataAspectRatio',[1 1 1])
    else
          set(gca,'DataAspectRatio',[1 1 1])
    end
    hold on;
    grid on;
end