#pragma once
#include "Model.h"
#include <stdarg.h>

using namespace MiniEngine::Rendering::Models;

class SkyBox :
	public Model
{
public:
	SkyBox();
	~SkyBox();

	void Create();
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) override final;
	virtual void Update() override final;
	void loadTexture(GLchar* filename);
private:

	GLuint m_texture;
};
