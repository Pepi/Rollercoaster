#pragma once

#include "GlewCore.h"
#include "IListener.h"
#include "WindowsInfo.h"
#include "ContextInfo.h"
#include "DebugOutput.h"
#include "FrameBufferInfo.h"
#include "Fps.h"

namespace MiniEngine {
	namespace Core {
		namespace Init {
			class GlfwCore
			{
			public:
				static void Init(const Core::WindowInfo& window,
					const Core::ContextInfo& context,
					const Core::FramebufferInfo& framebufferInfo);

			public:
				static void Run(void);
				static void Close();
				static void PrintOpenGLInfo(const Core::WindowInfo& windowInfo,
					const Core::ContextInfo& context);


			private:
				static void error_callback(int error, const char* description);
				static void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/);
				static void mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/);
				static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
				static void scroll_callback(GLFWwindow* window, double /*xoffset*/, double yoffset);
				static void ForwardInputs();
				static bool keys[1024];
				static double mouse_x, mouse_y;
				static double time;
				static IListener*   renderListener;
				static IListener*   physicListener;
				static WindowInfo   windowInformation;
				static GLFWwindow*	mWindow;
				static GLFWmonitor* mMainMonitor;
			public:
				static void SetRenderListener(Core::IListener* iListener);
				static void SetPhysicListener(Core::IListener* iListener);
				static int ShouldClose();

			};
		}
	}
}


