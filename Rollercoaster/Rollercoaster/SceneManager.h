#pragma once

#include "ShadersManager.h"
#include "ModelsManager.h"
#include "IListener.h"
#include "Camera.h"

namespace MiniEngine 
{
	namespace Managers
	{
		class SceneManager : public Core::IListener
		{
		public:
			SceneManager();
			~SceneManager();

			virtual void NotifyBeginFrame();
			virtual void NotifyDisplayFrame();
			virtual void NotifyEndFrame();
			virtual void NotifyReshape(int width, int height, int previos_width, int previous_height);
			virtual void NotifyInput(bool *key, double xpos, double ypos, double dt);

			void SetModelsManager(Managers::ModelsManager*& models_manager);

			Camera *GetCamera();

		private:
			Managers::ModelsManager* models_manager;

			glm::mat4 projection_matrix;
			glm::mat4 view_matrix;
			Camera *camera;
		};
	}
}



