#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <stdio.h>
#include <stdlib.h>

#include "App.h"

using namespace MiniEngine;

int main(void)
{
	App *rollercoaster = new App();

	if (rollercoaster->Initialize())
		return EXIT_FAILURE;

	if (rollercoaster->Load())
	return EXIT_FAILURE;

	rollercoaster->Launch();

	delete rollercoaster;

	return EXIT_SUCCESS;
}