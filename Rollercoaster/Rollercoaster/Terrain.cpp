#include "Terrain.h"

using namespace MiniEngine::Rendering;

#define XY_SCALE	1.0f
#define Z_SCALE		10.0f
#define TEX_SCALE	0.2f

Terrain::Terrain()
{
	mHeightMap = 0;
	mHasTessellation = 0;
	this->mBody = NULL;
}

Terrain::~Terrain()
{
}

bool Terrain::Initialize(GLchar *heightMapFile, GLchar *colorMapFile) {
	bool error;

	std::cout << "Load heightmap" << std::endl;
	error = LoadHeightMap(heightMapFile);
	if (error) {
		std::cout << "Unable to load heightmap" << std::endl;
		return EXIT_FAILURE;
	}

	SetTerrainCoordinates();

	std::cout << "Compute normals" << std::endl;
	error = CalculateNormals();
	if (error) {
		std::cout << "Error computing normals" << std::endl;
		return EXIT_FAILURE;
	}

	// Load in the color map for the terrain.

	std::cout << "Load colormap" << std::endl;
	error = LoadColorMap(colorMapFile);
	if (error)
	{
		std::cout << "Unable to load color map" << std::endl;
		return EXIT_FAILURE;
	}

	// Now build the 3D model of the terrain.
	std::cout << "Build terrain" << std::endl;
	error = BuildTerrainModel();
	if (error)
	{
		std::cout << "Error building terrain" << std::endl;
		return EXIT_FAILURE;
	}

	ShutdownHeightMap();

	std::cout << "Compute vectors" << std::endl;
	CalculateTerrainVectors();

	std::cout << "Initialize buffers" << std::endl;
	error = InitializeBuffers();
	if (error) {
		std::cout << "Unable to initialize object buffer" << std::endl;
		return EXIT_FAILURE;
	}

	ShutdownTerrainModel();

	std::cout << "Create terrain" << std::endl;
	this->Create();

	std::cout << "Load textures" << std::endl;
	this->loadTextures();

	btTransform t;
	t.setIdentity();
	t.setOrigin(btVector3(0.0f, 0.0f, 0.0f));
	btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0.0f, 1.0f, 0.0f), btScalar(200.0f));
	btMotionState* motion = new btDefaultMotionState(t);
	mBody = new btRigidBody(0.0f, motion, plane);

	return EXIT_SUCCESS;
}

btRigidBody *Terrain::GetBody() {
	return mBody;
}

bool Terrain::LoadHeightMap(GLchar *filename) {
	FILE* filePtr;
	int error;
	unsigned long count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	long imageSize, i, j, k, index;
	unsigned char* bitmapImage;
	unsigned char height;
	// Open the height map file in binary.
	error = fopen_s(&filePtr, filename, "rb");
	if (error != 0)
	{
		std::cout << "Unable to load terrain heightmap." << std::endl;
		return EXIT_FAILURE;
	}

	// Read in the file header.
	count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);
	if (count != 1)
	{
		std::cout << "Error loading heightmap: Header not found." << std::endl;
		return EXIT_FAILURE;
	}

	// Read in the bitmap info header.
	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);
	if (count != 1)
	{
		std::cout << "Error loading heightmap: File is empty." << std::endl;
		return EXIT_FAILURE;
	}

	// Save the dimensions of the terrain.
	mTerrainWidth = bitmapInfoHeader.biWidth;
	mTerrainHeight = bitmapInfoHeader.biHeight;
	imageSize = bitmapInfoHeader.biSizeImage;

	// Allocate memory for the bitmap image data.
	bitmapImage = new unsigned char[imageSize];
	if (!bitmapImage)
	{
		std::cout << "Error loading heightmap: unable to allocate image memory." << std::endl;
		return EXIT_FAILURE;
	}

	// Move to the beginning of the bitmap data.
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread(bitmapImage, 1, imageSize, filePtr);
	if (count != imageSize)
	{
		std::cout << "Error loading heightmap: file is corrupted." << std::endl;
		return EXIT_FAILURE;
	}

	// Close the file.
	error = fclose(filePtr);
	if (error != 0)
	{
		std::cout << "Error closing heightmap file." << std::endl;
		return EXIT_FAILURE;
	}
	
	// Create the structure to hold the height map data.
	mHeightMap = new HeightMapType[mTerrainWidth * mTerrainHeight];
	if (!mHeightMap)
	{
		std::cout << "Error loading heightmap: unable to allocate heightmap memory" << std::endl;
		return EXIT_FAILURE;
	}

	// Initialize the position in the image data buffer.
	k = 0;
	i = 0;
	index = 0;
	// Read the image data into the height map.
	for (j = 0; j<mTerrainHeight; j++)
	{
		i = 0;
		for (i = 0; i<mTerrainWidth; i++)
		{
			height = bitmapImage[k];


			mHeightMap[index].x = (float)i * XY_SCALE;
			mHeightMap[index].y = (float)height * XY_SCALE;
			mHeightMap[index].z = (float)j * XY_SCALE;

			index++;

			k += 3;
		}
		// Skip endl char from bitmap array
		k += 1;
	}

	// Release the bitmap image data.
	delete[] bitmapImage;
	bitmapImage = 0;

	return EXIT_SUCCESS;
}

void Terrain::SetTerrainCoordinates()
{
	long i, j, index;


	// Loop through all the elements in the height map array and adjust their coordinates correctly.
	for (j = 0; j<mTerrainHeight; j++)
	{
		for (i = 0; i<mTerrainWidth; i++)
		{
			index = (mTerrainWidth * j) + i;

			// Set the X and Z coordinates.
			mHeightMap[index].x = (float)i;
			mHeightMap[index].z = -(float)j;

			// Move the terrain depth into the positive range.  For example from (0, -256) to (256, 0).
			mHeightMap[index].z += (float)(mTerrainHeight - 1);

			// Scale the height.
			mHeightMap[index].y /= Z_SCALE;
		}
	}

	return;
}

bool Terrain::CalculateNormals()
{
	long i, j, index1, index2, index3, index;
	float vertex1[3], vertex2[3], vertex3[3], vector1[3], vector2[3], sum[3], length;
	VectorType* normals;


	// Create a temporary array to hold the face normal vectors.
	normals = new VectorType[(mTerrainHeight - 1) * (mTerrainWidth - 1)];
	if (!normals)
	{
		return EXIT_FAILURE;
	}

	// Go through all the faces in the mesh and calculate their normals.
	for (j = 0; j<(mTerrainHeight - 1); j++)
	{
		for (i = 0; i<(mTerrainWidth - 1); i++)
		{
			index1 = ((j + 1) * mTerrainWidth) + i;      // Bottom left vertex.
			index2 = ((j + 1) * mTerrainWidth) + (i + 1);  // Bottom right vertex.
			index3 = (j * mTerrainWidth) + i;          // Upper left vertex.

														// Get three vertices from the face.
			vertex1[0] = mHeightMap[index1].x;
			vertex1[1] = mHeightMap[index1].y;
			vertex1[2] = mHeightMap[index1].z;

			vertex2[0] = mHeightMap[index2].x;
			vertex2[1] = mHeightMap[index2].y;
			vertex2[2] = mHeightMap[index2].z;

			vertex3[0] = mHeightMap[index3].x;
			vertex3[1] = mHeightMap[index3].y;
			vertex3[2] = mHeightMap[index3].z;

			// Calculate the two vectors for this face.
			vector1[0] = vertex1[0] - vertex3[0];
			vector1[1] = vertex1[1] - vertex3[1];
			vector1[2] = vertex1[2] - vertex3[2];
			vector2[0] = vertex3[0] - vertex2[0];
			vector2[1] = vertex3[1] - vertex2[1];
			vector2[2] = vertex3[2] - vertex2[2];

			index = (j * (mTerrainWidth - 1)) + i;

			// Calculate the cross product of those two vectors to get the un-normalized value for this face normal.
			normals[index].x = (vector1[1] * vector2[2]) - (vector1[2] * vector2[1]);
			normals[index].y = (vector1[2] * vector2[0]) - (vector1[0] * vector2[2]);
			normals[index].z = (vector1[0] * vector2[1]) - (vector1[1] * vector2[0]);

			// Calculate the length.
			length = (float)sqrt((normals[index].x * normals[index].x) + (normals[index].y * normals[index].y) +
				(normals[index].z * normals[index].z));

			// Normalize the final value for this face using the length.
			normals[index].x = (normals[index].x / length);
			normals[index].y = (normals[index].y / length);
			normals[index].z = (normals[index].z / length);
		}
	}

	// Now go through all the vertices and take a sum of the face normals that touch this vertex.
	for (j = 0; j<mTerrainHeight; j++)
	{
		for (i = 0; i<mTerrainWidth; i++)
		{
			// Initialize the sum.
			sum[0] = 0.0f;
			sum[1] = 0.0f;
			sum[2] = 0.0f;

			// Bottom left face.
			if (((i - 1) >= 0) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (mTerrainWidth - 1)) + (i - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
			}

			// Bottom right face.
			if ((i<(mTerrainWidth - 1)) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (mTerrainWidth - 1)) + i;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
			}

			// Upper left face.
			if (((i - 1) >= 0) && (j<(mTerrainHeight - 1)))
			{
				index = (j * (mTerrainWidth - 1)) + (i - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
			}

			// Upper right face.
			if ((i < (mTerrainWidth - 1)) && (j < (mTerrainHeight - 1)))
			{
				index = (j * (mTerrainWidth - 1)) + i;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
			}

			// Calculate the length of this normal.
			length = (float)sqrt((sum[0] * sum[0]) + (sum[1] * sum[1]) + (sum[2] * sum[2]));

			// Get an index to the vertex location in the height map array.
			index = (j * mTerrainWidth) + i;

			// Normalize the final shared normal for this vertex and store it in the height map array.
			mHeightMap[index].nx = (sum[0] / length);
			mHeightMap[index].ny = (sum[1] / length);
			mHeightMap[index].nz = (sum[2] / length);

		}
	}


	// Release the temporary normals.
	delete[] normals;
	normals = 0;

	return EXIT_SUCCESS;
}

void Terrain::ShutdownHeightMap()
{
	if (mHeightMap)
	{
		delete[] mHeightMap;
		mHeightMap = 0;
	}

	return;
}

bool Terrain::LoadColorMap(GLchar *colorMapFile)
{
	int error;
	long imageSize, i, j, k, index;
	FILE* filePtr;
	unsigned long long count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	unsigned char* bitmapImage;


	// Open the color map file in binary.
	error = fopen_s(&filePtr, colorMapFile, "rb");
	if (error != 0)
	{
		return EXIT_FAILURE;
	}

	// Read in the file header.
	count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);
	if (count != 1)
	{
		return EXIT_FAILURE;
	}

	// Read in the bitmap info header.
	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);
	if (count != 1)
	{
		return EXIT_FAILURE;
	}

	// Make sure the color map dimensions are the same as the terrain dimensions for easy 1 to 1 mapping.
	if ((bitmapInfoHeader.biWidth != mTerrainWidth) || (bitmapInfoHeader.biHeight != mTerrainHeight))
	{
		return EXIT_FAILURE;
	}

	// Calculate the size of the bitmap image data.
	// Since this is non-divide by 2 dimensions (eg. 257x257) need to add extra byte to each line.
	imageSize = mTerrainHeight * ((mTerrainWidth * 3) + 1);

	// Allocate memory for the bitmap image data.
	bitmapImage = new unsigned char[imageSize];
	if (!bitmapImage)
	{
		return EXIT_FAILURE;
	}

	// Move to the beginning of the bitmap data.
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread(bitmapImage, 1, imageSize, filePtr);
	if (count != imageSize)
	{
		return EXIT_FAILURE;
	}

	// Close the file.
	error = fclose(filePtr);
	if (error != 0)
	{
		return EXIT_FAILURE;
	}

	// Initialize the position in the image data buffer.
	k = 0;

	// Read the image data into the color map portion of the height map structure.
	for (j = 0; j<mTerrainHeight; j++)
	{
		for (i = 0; i<mTerrainWidth; i++)
		{
			// Bitmaps are upside down so load bottom to top into the array.
			index = (mTerrainWidth * (mTerrainHeight - 1 - j)) + i;

			mHeightMap[index].b = (float)bitmapImage[k] / 255.0f;
			mHeightMap[index].g = (float)bitmapImage[k + 1] / 255.0f;
			mHeightMap[index].r = (float)bitmapImage[k + 2] / 255.0f;

			k += 3;
		}

		// Compensate for extra byte at end of each line in non-divide by 2 bitmaps (eg. 257x257).
		k++;
	}

	// Release the bitmap image data.
	delete[] bitmapImage;
	bitmapImage = 0;

	return EXIT_SUCCESS;
}

bool Terrain::BuildTerrainModel()
{
	long i, j, index, index1, index2, index3, index4;


	// Calculate the number of vertices in the 3D terrain model.
	mVertexCount = (mTerrainHeight - 1) * (mTerrainWidth - 1) * 6;

	// Create the 3D terrain model array.
	mTerrainModel = new ModelType[mVertexCount];
	if (!mTerrainModel)
	{
		return EXIT_FAILURE;
	}

	// Initialize the index into the height map array.
	index = 0;

	// Load the 3D terrain model with the height map terrain data.
	// We will be creating 2 triangles for each of the four points in a quad.
	for (j = 0; j<(mTerrainHeight - 1); j++)
	{
		for (i = 0; i<(mTerrainWidth - 1); i++)
		{
			// Get the indexes to the four points of the quad.
			index1 = (mTerrainWidth * j) + i;          // Upper left.
			index2 = (mTerrainWidth * j) + (i + 1);      // Upper right.
			index3 = (mTerrainWidth * (j + 1)) + i;      // Bottom left.
			index4 = (mTerrainWidth * (j + 1)) + (i + 1);  // Bottom right.

															// Now create two triangles for that quad.
															// Triangle 1 - Upper left.
			mTerrainModel[index].x = mHeightMap[index1].x;
			mTerrainModel[index].y = mHeightMap[index1].y;
			mTerrainModel[index].z = mHeightMap[index1].z;
			mTerrainModel[index].tu = mHeightMap[index1].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index1].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index1].nx;
			mTerrainModel[index].ny = mHeightMap[index1].ny;
			mTerrainModel[index].nz = mHeightMap[index1].nz;
			mTerrainModel[index].r = mHeightMap[index1].r;
			mTerrainModel[index].g = mHeightMap[index1].g;
			mTerrainModel[index].b = mHeightMap[index1].b;
			index++;

			// Triangle 1 - Upper right.
			mTerrainModel[index].x = mHeightMap[index2].x;
			mTerrainModel[index].y = mHeightMap[index2].y;
			mTerrainModel[index].z = mHeightMap[index2].z;
			mTerrainModel[index].tu = mHeightMap[index2].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index2].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index2].nx;
			mTerrainModel[index].ny = mHeightMap[index2].ny;
			mTerrainModel[index].nz = mHeightMap[index2].nz;
			mTerrainModel[index].r = mHeightMap[index2].r;
			mTerrainModel[index].g = mHeightMap[index2].g;
			mTerrainModel[index].b = mHeightMap[index2].b;
			index++;

			// Triangle 1 - Bottom left.
			mTerrainModel[index].x = mHeightMap[index3].x;
			mTerrainModel[index].y = mHeightMap[index3].y;
			mTerrainModel[index].z = mHeightMap[index3].z;
			mTerrainModel[index].tu = mHeightMap[index3].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index3].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index3].nx;
			mTerrainModel[index].ny = mHeightMap[index3].ny;
			mTerrainModel[index].nz = mHeightMap[index3].nz;
			mTerrainModel[index].r = mHeightMap[index3].r;
			mTerrainModel[index].g = mHeightMap[index3].g;
			mTerrainModel[index].b = mHeightMap[index3].b;
			index++;

			// Triangle 2 - Bottom left.
			mTerrainModel[index].x = mHeightMap[index3].x;
			mTerrainModel[index].y = mHeightMap[index3].y;
			mTerrainModel[index].z = mHeightMap[index3].z;
			mTerrainModel[index].tu = mHeightMap[index3].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index3].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index3].nx;
			mTerrainModel[index].ny = mHeightMap[index3].ny;
			mTerrainModel[index].nz = mHeightMap[index3].nz;
			mTerrainModel[index].r = mHeightMap[index3].r;
			mTerrainModel[index].g = mHeightMap[index3].g;
			mTerrainModel[index].b = mHeightMap[index3].b;
			index++;

			// Triangle 2 - Upper right.
			mTerrainModel[index].x = mHeightMap[index2].x;
			mTerrainModel[index].y = mHeightMap[index2].y;
			mTerrainModel[index].z = mHeightMap[index2].z;
			mTerrainModel[index].tu = mHeightMap[index2].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index2].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index2].nx;
			mTerrainModel[index].ny = mHeightMap[index2].ny;
			mTerrainModel[index].nz = mHeightMap[index2].nz;
			mTerrainModel[index].r = mHeightMap[index2].r;
			mTerrainModel[index].g = mHeightMap[index2].g;
			mTerrainModel[index].b = mHeightMap[index2].b;
			index++;

			// Triangle 2 - Bottom right.
			mTerrainModel[index].x = mHeightMap[index4].x;
			mTerrainModel[index].y = mHeightMap[index4].y;
			mTerrainModel[index].z = mHeightMap[index4].z;
			mTerrainModel[index].tu = mHeightMap[index4].x * TEX_SCALE;
			mTerrainModel[index].tv = mHeightMap[index4].z * TEX_SCALE;
			mTerrainModel[index].nx = mHeightMap[index4].nx;
			mTerrainModel[index].ny = mHeightMap[index4].ny;
			mTerrainModel[index].nz = mHeightMap[index4].nz;
			mTerrainModel[index].r = mHeightMap[index4].r;
			mTerrainModel[index].g = mHeightMap[index4].g;
			mTerrainModel[index].b = mHeightMap[index4].b;
			index++;
		}
	}

	return EXIT_SUCCESS;
}


void Terrain::ShutdownTerrainModel()
{
	// Release the terrain model data.
	if (mTerrainModel)
	{
		delete[] mTerrainModel;
		mTerrainModel = 0;
	}

	return;
}

void Terrain::CalculateTerrainVectors()
{
	long faceCount, i, index;
	TempVertexType vertex1, vertex2, vertex3;
	VectorType tangent, binormal;


	// Calculate the number of faces in the terrain model.
	faceCount = mVertexCount / 3;

	// Initialize the index to the model data.
	index = 0;

	// Go through all the faces and calculate the the tangent, binormal, and normal vectors.
	for (i = 0; i<faceCount; i++)
	{
		// Get the three vertices for this face from the terrain model.
		vertex1.x = mTerrainModel[index].x;
		vertex1.y = mTerrainModel[index].y;
		vertex1.z = mTerrainModel[index].z;
		vertex1.tu = mTerrainModel[index].tu;
		vertex1.tv = mTerrainModel[index].tv;
		vertex1.nx = mTerrainModel[index].nx;
		vertex1.ny = mTerrainModel[index].ny;
		vertex1.nz = mTerrainModel[index].nz;
		index++;

		vertex2.x = mTerrainModel[index].x;
		vertex2.y = mTerrainModel[index].y;
		vertex2.z = mTerrainModel[index].z;
		vertex2.tu = mTerrainModel[index].tu;
		vertex2.tv = mTerrainModel[index].tv;
		vertex2.nx = mTerrainModel[index].nx;
		vertex2.ny = mTerrainModel[index].ny;
		vertex2.nz = mTerrainModel[index].nz;
		index++;

		vertex3.x = mTerrainModel[index].x;
		vertex3.y = mTerrainModel[index].y;
		vertex3.z = mTerrainModel[index].z;
		vertex3.tu = mTerrainModel[index].tu;
		vertex3.tv = mTerrainModel[index].tv;
		vertex3.nx = mTerrainModel[index].nx;
		vertex3.ny = mTerrainModel[index].ny;
		vertex3.nz = mTerrainModel[index].nz;
		index++;

		// Calculate the tangent and binormal of that face.
		CalculateTangentBinormal(vertex1, vertex2, vertex3, tangent, binormal);

		// Store the tangent and binormal for this face back in the model structure.
		mTerrainModel[index - 1].tx = tangent.x;
		mTerrainModel[index - 1].ty = tangent.y;
		mTerrainModel[index - 1].tz = tangent.z;
		mTerrainModel[index - 1].bx = binormal.x;
		mTerrainModel[index - 1].by = binormal.y;
		mTerrainModel[index - 1].bz = binormal.z;

		mTerrainModel[index - 2].tx = tangent.x;
		mTerrainModel[index - 2].ty = tangent.y;
		mTerrainModel[index - 2].tz = tangent.z;
		mTerrainModel[index - 2].bx = binormal.x;
		mTerrainModel[index - 2].by = binormal.y;
		mTerrainModel[index - 2].bz = binormal.z;

		mTerrainModel[index - 3].tx = tangent.x;
		mTerrainModel[index - 3].ty = tangent.y;
		mTerrainModel[index - 3].tz = tangent.z;
		mTerrainModel[index - 3].bx = binormal.x;
		mTerrainModel[index - 3].by = binormal.y;
		mTerrainModel[index - 3].bz = binormal.z;
	}

	return;
}

void Terrain::CalculateTangentBinormal(TempVertexType vertex1, TempVertexType vertex2, TempVertexType vertex3, VectorType& tangent, VectorType& binormal)
{
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;

	// Calculate the two vectors for this face.
	vector1[0] = vertex2.x - vertex1.x;
	vector1[1] = vertex2.y - vertex1.y;
	vector1[2] = vertex2.z - vertex1.z;

	vector2[0] = vertex3.x - vertex1.x;
	vector2[1] = vertex3.y - vertex1.y;
	vector2[2] = vertex3.z - vertex1.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = vertex2.tu - vertex1.tu;
	tvVector[0] = vertex2.tv - vertex1.tv;

	tuVector[1] = vertex3.tu - vertex1.tu;
	tvVector[1] = vertex3.tv - vertex1.tv;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of the tangent.
	length = (float)sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));

	// Normalize the tangent and then store it.
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of the binormal.
	length = (float)sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));

	// Normalize the binormal and then store it.
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	return;
}

bool Terrain::InitializeBuffers()
{
	mHasTessellation = true;
	unsigned long* indices;
	long i;

	HRESULT result;
	long index1, index2, index3, index4;
	// Calculate the number of vertices in the terrain mesh.
	mVertexSize = (mTerrainWidth - 1) * (mTerrainHeight - 1) * 6;

	// Set the index count to the same as the vertex count.
	mIndexCount = mVertexSize;
	
	// Create the vertex array.
	mVertex = new VertexFormat[mVertexSize];
	if (!mVertex)
	{
		return EXIT_FAILURE;
	}

	// Create the index array.
	indices = new unsigned long[mIndexCount];
	if (!indices)
	{
		return EXIT_FAILURE;
	}

	// Load the vertex and index array with the terrain data.
	for (i = 0; i<mVertexCount; i++) {
		mVertex[i].position = glm::vec3(mTerrainModel[i].x, mTerrainModel[i].y, mTerrainModel[i].z);
		mVertex[i].color = glm::vec4(mTerrainModel[i].r, mTerrainModel[i].g, mTerrainModel[i].b, 1.0f);
		mVertex[i].texture = glm::vec2(mTerrainModel[i].tu, mTerrainModel[i].tv);
		mVertex[i].normal = glm::vec3(mTerrainModel[i].nx, mTerrainModel[i].ny, mTerrainModel[i].nz);
		mVertex[i].tangent = glm::vec3(mTerrainModel[i].tx, mTerrainModel[i].ty, mTerrainModel[i].tz);
		mVertex[i].binormal = glm::vec3(mTerrainModel[i].bx, mTerrainModel[i].by, mTerrainModel[i].bz);
		indices[i] = i;
	}


	return EXIT_SUCCESS;
}

bool Terrain::loadTextures(){

	GLuint color, normal, glossiness;

	//create the OpenGL texture
	color = SOIL_load_OGL_texture("Images/Terrain/Tundra/Color.jpg", SOIL_LOAD_RGBA, 0, SOIL_FLAG_MIPMAPS);
	normal = SOIL_load_OGL_texture("Images/Terrain/Tundra/Normal.jpg", SOIL_LOAD_RGBA, 0, SOIL_FLAG_MIPMAPS);
	glossiness = SOIL_load_OGL_texture("Images/Terrain/Tundra/Glossiness.jpg", SOIL_LOAD_RGBA, 0, SOIL_FLAG_MIPMAPS);

	this->SetTexture("ColorTexture", color);
	this->SetTexture("NormalTexture", normal);
	this->SetTexture("GlossinessTexture", glossiness);

	mHasColorTex = true;
	mHasNormalTex = true;
	mHasGlossTex = true;

	return EXIT_SUCCESS;
}