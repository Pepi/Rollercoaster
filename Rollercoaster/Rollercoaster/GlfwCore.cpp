#include "GlfwCore.h"


using namespace MiniEngine;
using namespace Core;
using namespace Init;

Core::IListener* GlfwCore::renderListener = NULL;
Core::IListener* GlfwCore::physicListener = NULL;
Core::WindowInfo GlfwCore::windowInformation;

GLFWwindow* GlfwCore::mWindow;
GLFWmonitor* GlfwCore::mMainMonitor;

bool GlfwCore::keys[1024];
double GlfwCore::mouse_x;
double GlfwCore::mouse_y;
double GlfwCore::time;

void GlfwCore::Init(const Core::WindowInfo& window, const Core::ContextInfo& context, const Core::FramebufferInfo& framebufferInfo) {

	windowInformation = window;

	//Set the error callback  
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW  
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional  
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context.major_version); //Request a specific OpenGL version  
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context.minor_version); //Request a specific OpenGL version  
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing  
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	//Declare a window object  

	//Create a window and create its OpenGL context
	mMainMonitor = glfwGetPrimaryMonitor();
	const GLFWvidmode * mode = glfwGetVideoMode(mMainMonitor);
	if (window.fullscreen == true) {
		
		mWindow = glfwCreateWindow(mode->width, mode->height, windowInformation.name, mMainMonitor, NULL);
	}
	else {
		mWindow = glfwCreateWindow(window.width, window.height, windowInformation.name, NULL, NULL);
	}
	std::cout << "Monitor name:	" << glfwGetMonitorName(mMainMonitor) << std::endl;
	std::cout << "Monitor resolution: " << mode->width << "X" << mode->height << std::endl;

	//This function makes the context of the specified window current on the calling thread.   
	glfwMakeContextCurrent(mWindow);

	//Sets the key callback  
	glfwSetKeyCallback(mWindow, key_callback);
	glfwSetCursorPosCallback(mWindow, mouse_callback);
	glfwSetMouseButtonCallback(mWindow, mouse_button_callback);
	glfwSetScrollCallback(mWindow, scroll_callback);

	// Disable cursor and center it
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPos(mWindow, window.width/2.0, window.height/2.0);
	glfwFocusWindow(mWindow);

	GlewCore::Init();

	glEnable(GL_DEBUG_OUTPUT); glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	if (GL_DEBUG_OUTPUT)
	{
		std::cout << "Debug Output supported" << std::endl;
	}

	// Enable VSYNc by default
	glfwSwapInterval(1);

	//If the window couldn't be created  
	if (!mWindow)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	PrintOpenGLInfo(window, context);

}

void GlfwCore::Run(void) {
	
	Fps::Initialize();

	while (!ShouldClose())
	{
		Fps::Frame();
		if (renderListener)
		{

			renderListener->NotifyInput(keys, mouse_x, mouse_y, 1.0/Fps::GetFps());
			glfwSetCursorPos(mWindow, windowInformation.width / 2.0, windowInformation.height / 2.0);

			renderListener->NotifyBeginFrame();
			physicListener->NotifyBeginFrame();
			renderListener->NotifyDisplayFrame();

			glfwSwapBuffers(mWindow);

			renderListener->NotifyEndFrame();
		}

		glfwPollEvents();
	}
	Close();
}

void GlfwCore::Close() {
	//Close OpenGL window and terminate GLFW  
	glfwDestroyWindow(mWindow);
	//Finalize and clean up GLFW  
	glfwTerminate();
	std::cout << "GLUT:\t Finished" << std::endl;
}

void GlfwCore::PrintOpenGLInfo(const Core::WindowInfo& windowInfo, const Core::ContextInfo& contextInfo) {
	const unsigned char* renderer = glGetString(GL_RENDERER);
	const unsigned char* vendor = glGetString(GL_VENDOR);
	const unsigned char* version = glGetString(GL_VERSION);

	std::cout << "*******************************************************************************" << std::endl;

	std::cout << "GLUT:\tVendor : " << vendor << std::endl;
	std::cout << "GLUT:\tRenderer : " << renderer << std::endl;
	std::cout << "GLUT:\tOpenGl version: " << version << std::endl;
	std::cout << "GLUT:\tInitial window is '" << windowInfo.name << "', with dimensions (" << windowInfo.width
		<< "X" << windowInfo.height;
	std::cout << ") starts at (" << windowInfo.position_x << "X" << windowInfo.position_y;
	std::cout << ") and " << ((windowInfo.isReshapable) ? "is" : "is not ") << " redimensionable" << std::endl;
	std::cout << "GLUT:\tInitial Framebuffer contains double buffers for" << std::endl;

	std::cout << "GLUT:\t OpenGL context is " << contextInfo.major_version << "." << contextInfo.minor_version;
	std::cout << " and profile is " << ((contextInfo.core) ? "core" : "compatibility") << std::endl;

	std::cout << "*****************************************************************" << std::endl;
}


void GlfwCore::SetRenderListener(Core::IListener* iListener) {
	renderListener = iListener;
	renderListener->NotifyReshape(windowInformation.width, windowInformation.height, 0, 0);
}


void GlfwCore::SetPhysicListener(Core::IListener* iListener) {
	physicListener = iListener;
}

int GlfwCore::ShouldClose() {
	return glfwWindowShouldClose(mWindow);
}

//Define an error callback  
void GlfwCore::error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}


//Define the key input callback  
void GlfwCore::key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;

	// Quit
	if (keys[GLFW_KEY_ESCAPE])
		glfwSetWindowShouldClose(window, GL_TRUE);

	// V-SYNC
	if (keys[GLFW_KEY_U]) {
		static bool vsync = false;
		if (vsync) {
			glfwSwapInterval(1);
		}
		else {
			glfwSwapInterval(0);
		}
		vsync = !vsync;
	}
}

void GlfwCore::ForwardInputs()
{
	double dt = 1.0f/Fps::GetFps();
	renderListener->NotifyInput(keys, mouse_x, mouse_y, dt);
}

void GlfwCore::mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_RIGHT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_RIGHT] = false;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_LEFT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_LEFT] = false;

	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = true;
	else
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = false;
}

void GlfwCore::mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	mouse_x = xpos - (double)windowInformation.width / 2.0;
	mouse_y = ypos - (double)windowInformation.height / 2.0;

	if (keys[GLFW_MOUSE_BUTTON_RIGHT]) {
		std::cout << "Mouse Position : (" << xpos << ", " << ypos << ")" << std::endl;
	}
	
}

void GlfwCore::scroll_callback(GLFWwindow* window, double /*xoffset*/, double yoffset)
{
	if (keys[GLFW_MOUSE_BUTTON_LEFT]) {
		std::cout << "Mouse Offset : " << yoffset << std::endl;
	}
}