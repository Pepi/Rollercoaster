#pragma once
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace MiniEngine
{
	namespace Managers
	{

		class ShadersManager
		{

		public:

			ShadersManager(void);
			~ShadersManager(void);

			void CreateProgram(const GLchar *shaderName,
				const GLchar *vertexPath,
				const GLchar *fragmentPath,
				const GLchar *geometryPath,
				const GLchar *tessCPath,
				const GLchar *tessEPat);
			GLuint pathToShader(const GLchar* path, GLenum shaderType);
			void checkCompileErrors(const GLuint &object, std::string type);
			std::string shaderTypeToString(GLenum shaderType);

			static const GLuint GetShader(const std::string&);

		private:

			std::string ReadShader(const std::string& filename);

			static std::map<std::string, GLuint> programs;
		};
	}
}