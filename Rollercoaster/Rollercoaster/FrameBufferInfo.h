#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace MiniEngine
{
	namespace Core
	{
		struct FramebufferInfo
		{
			unsigned int flags;
			bool msaa;

			FramebufferInfo()
			{

				flags = GLFW_SRGB_CAPABLE | GLFW_DOUBLEBUFFER | GLFW_DEPTH_BITS;
				msaa = false;
			}

			FramebufferInfo(bool color, bool depth, bool stencil, bool msaa)
			{

				flags = GLFW_DOUBLEBUFFER;
				if (color)
					flags |= GLFW_SRGB_CAPABLE | GLFW_ALPHA_BITS;

				if (depth)
					flags |= GLFW_DEPTH_BITS;

				if (stencil)
					flags |= GLFW_STENCIL_BITS;

				if (msaa)

					flags |= GLFW_SAMPLES;
				this->msaa = msaa;
			}

			void operator=(const FramebufferInfo& info)
			{
				flags = info.flags;
				msaa = info.msaa;
			}
		};
	}
}
