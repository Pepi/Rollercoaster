#include "App.h"

using namespace MiniEngine::Rendering::Models;

App::App()
{
	engine = 0;
}


App::~App()
{
	delete engine;
}

bool App::Initialize() {

	std::cout << "Initialize Engine" << std::endl;
	engine = new MiniEngine::Engine();

	if (engine->Init())
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

bool App::Load() {

	std::cout << "Load Shaders" << std::endl;
	engine->GetShader_Manager()->CreateProgram("SkyDomeShader", "./Shaders/skydome.vert", "./Shaders/skydome.frag", NULL, NULL, NULL);
	engine->GetShader_Manager()->CreateProgram("genericShader", "./Shaders/generic.vs", "./Shaders/generic.fs", NULL, "./Shaders/generic.cs", "./Shaders/generic.es");
	engine->GetShader_Manager()->CreateProgram("terrainShader", "./Shaders/terrain.vert", "./Shaders/terrain.frag", NULL, "./Shaders/terrain.cs", "./Shaders/terrain.es");

	std::cout << "Load SkyDome" << std::endl;
	SkyDome *skydome = new SkyDome();
	skydome->Initialize();
	skydome->ListenToPosition(&(engine->GetScene_Manager()->GetCamera()->Position));

	std::cout << "Load Train" << std::endl;
	Train *train = new Train();
	train->Initialize();
	train->SetCamera(engine->GetScene_Manager()->GetCamera());

	std::cout << "Load Rollercoaster" << std::endl;
	Rollercoaster *rollercoaster = new Rollercoaster();
	rollercoaster->Initialize("./Objects/Rollercoaster.obj", "./Objects/Rollercoaster.trac");
	rollercoaster->SetTrain(train);

	std::cout << "Load Terrain" << std::endl;
	Terrain *terrain = new Terrain();
	terrain->Initialize("./Images/Terrain/heightmap.bmp", "./Images/Terrain/colormap.bmp");

	std::cout << "Load Ball" << std::endl;
	Ball *ball = new Ball(1.0f, glm::vec3(116.0f, 8.0f, 116.0f));
	ball->Initialize("./Objects/sphere.obj");


	std::cout << "Attach Shaders" << std::endl;
	int program;
	program = engine->GetShader_Manager()->GetShader("SkyDomeShader");
	skydome->SetProgram(program);
	program = engine->GetShader_Manager()->GetShader("genericShader");
	rollercoaster->SetProgram(program);
	train->SetProgram(program);
	ball->SetProgram(program);
	program = engine->GetShader_Manager()->GetShader("terrainShader");
	terrain->SetProgram(program);

	std::cout << "Attach Models to Rendering list" << std::endl;
	engine->GetModels_Manager()->SetModel("1-skydome", skydome); // Needs to be rendered first because no depht test
	engine->GetModels_Manager()->SetModel("rollercoaster", rollercoaster);
	engine->GetModels_Manager()->SetModel("train", train);
	engine->GetModels_Manager()->SetModel("terrain", terrain);
	engine->GetModels_Manager()->SetModel("ball", ball);

	std::cout << "Attach Bodies to Collision list" << std::endl;
	engine->GetCollision_Manager()->AddRigidBody(ball->GetBody());
	engine->GetCollision_Manager()->AddRigidBody(terrain->GetBody());

	return EXIT_SUCCESS;
}

void App::Launch() {
	engine->Run();
}