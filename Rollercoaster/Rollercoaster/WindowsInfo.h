#pragma once
#include <string>
namespace MiniEngine
{
	namespace Core
	{

		struct WindowInfo
		{


			GLchar *name;
			GLchar * monitorName;
			int width, height;
			int position_x, position_y;
			bool isReshapable;
			bool fullscreen;

			WindowInfo()
			{

				name = "OpenGL tutorial";
				width = 800; height = 600;
				position_x = 300;
				position_y = 300;
				isReshapable = false;
				fullscreen = false;
			}

			WindowInfo(GLchar *name, int start_position_x, int start_position_y, int width, int height, bool is_fullscreen, bool is_reshapable)
			{

				this->name = name;

				this->position_x = start_position_x;
				this->position_y = start_position_y;

				this->width = width;
				this->height = height;

				this->fullscreen = is_fullscreen;
				this->isReshapable = is_reshapable;
			}

			WindowInfo(const WindowInfo& windowInfo)
			{

				name = windowInfo.name;

				position_x = windowInfo.position_x;
				position_y = windowInfo.position_y;

				width = windowInfo.width;
				height = windowInfo.height;

				isReshapable = windowInfo.isReshapable;
			}

			void operator=(const WindowInfo& windowInfo)
			{

				name = windowInfo.name;

				position_x = windowInfo.position_x;
				position_y = windowInfo.position_y;

				width = windowInfo.width;
				height = windowInfo.height;

				isReshapable = windowInfo.isReshapable;
			}

		};

	}
}