#include "SceneManager.h"


using namespace MiniEngine;
using namespace Managers;

SceneManager::SceneManager()
{
	camera = new Camera(glm::vec3(100.0f, 20.0f, 100.0f));
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.85f, 0.85f, 0.85f, 0.0f);
	view_matrix = camera->GetViewMatrix();
}

SceneManager::~SceneManager()
{
}

void SceneManager::NotifyBeginFrame()
{
	view_matrix = camera->GetViewMatrix();
	models_manager->Update();
}

void SceneManager::NotifyDisplayFrame()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.85f, 0.85f, 0.85f, 0.0f);

	models_manager->Draw();
	models_manager->Draw(projection_matrix, view_matrix, camera->Position);
}

void SceneManager::NotifyEndFrame()
{

}

void SceneManager::NotifyReshape(int width, int height,
	int previos_width, int previous_height)
{
	float ar = width / height;
	float near1 = 0.1f, far1 = 20000.0f;

	projection_matrix = glm::perspective(camera->Zoom, ar, near1, far1);
}

void SceneManager::SetModelsManager(Managers::ModelsManager*& models_m)
{
	models_manager = models_m;
}

Camera *SceneManager::GetCamera() {
	return camera;
}

void SceneManager::NotifyInput(bool *keys, double xpos, double ypos, double dt) {
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, dt);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, dt);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, dt);
	if (keys[GLFW_KEY_D]) {
		camera->ProcessKeyboard(RIGHT, dt);
	}

	if (keys[GLFW_KEY_L]) {
		camera->Locked = !camera->Locked;
	}

	camera->ProcessMouseMovement(xpos, -ypos);
}