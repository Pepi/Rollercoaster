#pragma once
#include <stdio.h>
#include <Windows.h>

#include "GenericObject.h"
#include "TextureLoader.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

using namespace MiniEngine::Rendering::Models;

class Terrain :
	public GenericObject
{
private:
	struct HeightMapType
	{
		GLfloat x, y, z;
		GLfloat nx, ny, nz;
		GLfloat r, g, b;
	};

	struct ModelType
	{
		GLfloat x, y, z;
		GLfloat tu, tv;
		GLfloat nx, ny, nz;
		GLfloat tx, ty, tz;
		GLfloat bx, by, bz;
		GLfloat r, g, b;
	};

	struct VectorType
	{
		GLfloat x, y, z;
	};

	struct TempVertexType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

public:
	Terrain();
	~Terrain();

	bool Initialize(GLchar *heightMapFile, GLchar *colorMapFile);

	btRigidBody *GetBody();

private:
	bool LoadHeightMap(GLchar *heightMapFile);
	void SetTerrainCoordinates();
	bool CalculateNormals();
	void ShutdownHeightMap();
	bool InitializeBuffers();
	bool loadTextures();
	bool LoadColorMap(GLchar *colorMapFile);
	bool BuildTerrainModel();
	void ShutdownTerrainModel();
	void CalculateTerrainVectors();
	void CalculateTangentBinormal(TempVertexType, TempVertexType, TempVertexType, VectorType&, VectorType&);

	GLuint64 mVertexCount, mIndexCount;
	GLuint64 mTerrainWidth, mTerrainHeight;
	HeightMapType *mHeightMap;
	ModelType* mTerrainModel;

	btRigidBody* mBody;
};

