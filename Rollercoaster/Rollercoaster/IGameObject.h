#pragma once
#include <vector>
#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "VertexFormat.h"
#include "TextureLoader.h"

namespace MiniEngine
{
	namespace Rendering
	{
		class IGameObject
		{
		public:
			virtual ~IGameObject() = 0;

			virtual void Draw() = 0;
			virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) = 0;
			virtual void Update() = 0;
			virtual void SetProgram(GLuint shaderName) = 0;
			virtual void Destroy() = 0;

			virtual GLuint GetVao() const = 0;
			virtual const std::vector<GLuint> GetVbos() const = 0;

			virtual void SetTexture(std::string textureName, GLuint texture) = 0;
			virtual const GLuint GetTexture(std::string textureName) const = 0;

			virtual void SetPosition(float x, float y, float z) = 0;

		};

		inline IGameObject::~IGameObject()
		{
		}
	}
}