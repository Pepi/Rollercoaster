#pragma once
#include <unordered_map>
#include "ShadersManager.h"
#include "IGameObject.h"

namespace MiniEngine
{
	using namespace Rendering;
	namespace Managers
	{
		class ModelsManager
		{
		public:
			ModelsManager();
			~ModelsManager();

			void Draw();
			void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position);
			void Update();

			void DeleteModel(const std::string& gameModelName);
			const IGameObject& GetModel(const std::string& gameModelName) const;

			void DeleteModel_NDC(const std::string& gameModelName);
			const IGameObject& GetModel_NDC(const std::string& gameModelName) const;

			void SetModel(const std::string& gameObjectName, IGameObject* gameObject);

		private:
			std::map<std::string, IGameObject*> gameModelList;
			std::map<std::string, IGameObject*> gameModelList_NDC;
		};
	}
}
