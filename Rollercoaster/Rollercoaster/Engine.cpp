#include "Engine.h"

using namespace MiniEngine;
using namespace MiniEngine::Core;

Engine::Engine()
{
}

bool Engine::Init() {
	
	WindowInfo window((GLchar*)("Pepi's Rollecoaster"), 0, 0, 1200, 800, false, false);
	ContextInfo context(4, 3, true);
	FramebufferInfo frameBufferInfo(true, true, true, true);

	Init::GlfwCore::Init(window, context, frameBufferInfo);

	m_scene_manager = new Managers::SceneManager();
	m_collision_manager = new Managers::CollisionManager();
	m_shader_manager = new Managers::ShadersManager();	

	if (m_scene_manager && m_shader_manager)
	{
		m_models_manager = new Managers::ModelsManager();
		m_scene_manager->SetModelsManager(m_models_manager);
	}
	else
	{
		return false;
	}

	Init::GlfwCore::SetRenderListener(m_scene_manager);
	Init::GlfwCore::SetPhysicListener(m_collision_manager);

	return EXIT_SUCCESS;
}

void Engine::Run() {
	Init::GlfwCore::Run();
}

Managers::SceneManager* Engine::GetScene_Manager() const
{
	return m_scene_manager;
}

Managers::ShadersManager* Engine::GetShader_Manager() const
{
	return m_shader_manager;
}

Managers::ModelsManager* Engine::GetModels_Manager() const
{
	return m_models_manager;
}

Managers::CollisionManager* Engine::GetCollision_Manager() const
{
	return m_collision_manager;
}

TextureLoader* Engine::GetTexture_Loader() const
{
	return m_texture_loader;
}

Engine::~Engine()
{
	if (m_scene_manager)
		delete m_scene_manager;

	if (m_shader_manager)
		delete m_shader_manager;

	if (m_models_manager)
		delete m_models_manager;

	if (m_texture_loader)
		delete m_texture_loader;

	if (m_collision_manager)
		delete m_collision_manager;
}