#include "ShadersManager.h"
using namespace MiniEngine;
using namespace Managers;

std::map<std::string, GLuint> ShadersManager::programs;

ShadersManager::ShadersManager(void) {}

ShadersManager::~ShadersManager(void)
{

	for (std::map<std::string, GLuint>::iterator i = programs.begin();
	i != programs.end(); ++i)
	{
		GLuint pr = i->second;
		glDeleteProgram(pr);
	}
	programs.clear();
}

std::string ShadersManager::ReadShader(const std::string& filename)
{

	std::string shaderCode;
	std::ifstream file(filename, std::ios::in);

	if (!file.good()) {
		std::cout << "Can't read file " << filename.c_str() << std::endl;
		std::terminate();
	}

	file.seekg(0, std::ios::end);
	shaderCode.resize((unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&shaderCode[0], shaderCode.size());
	file.close();

	return shaderCode;
}

void ShadersManager::CreateProgram(const GLchar *shaderName, const GLchar *vertexPath, const GLchar *fragmentPath, const GLchar *geometryPath, const GLchar *tessCPath, const GLchar *tessEPath)
{

	GLuint vertexShader = pathToShader(vertexPath, GL_VERTEX_SHADER);
	GLuint fragmentShader = pathToShader(fragmentPath, GL_FRAGMENT_SHADER);
	GLuint geometryShader = (geometryPath) ? pathToShader(geometryPath, GL_GEOMETRY_SHADER) : 0;
	GLuint tessCShader = (tessCPath) ? pathToShader(tessCPath, GL_TESS_CONTROL_SHADER) : 0;
	GLuint tessEShader = (tessEPath) ? pathToShader(tessEPath, GL_TESS_EVALUATION_SHADER) : 0;

	// Create Program
	GLuint program = glCreateProgram();

	// Attach the shaders to the program
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	if (geometryPath) glAttachShader(program, geometryShader);
	if (tessCPath) glAttachShader(program, tessCShader);
	if (tessEPath) glAttachShader(program, tessEShader);

	glLinkProgram(program);
	checkCompileErrors(program, "Program");

	// Delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	if (geometryPath) glDeleteShader(geometryShader);
	if (tessCPath) glDeleteShader(tessCShader);

	programs[shaderName] = program;
}

GLuint ShadersManager::pathToShader(const GLchar* path, GLenum shaderType) {
	std::ifstream shaderFile;
	std::stringstream shaderStream;

	shaderFile.open(path);

	if (!shaderFile.good()) {
		std::cout << "Can't read file " << path << std::endl;
		std::terminate();
	}


	shaderStream << shaderFile.rdbuf();
	shaderFile.close();

	std::string code = shaderStream.str();
	const GLchar * ccode = code.c_str();

	GLuint shader;
	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &ccode, NULL);
	glCompileShader(shader);
	checkCompileErrors(shader, shaderTypeToString(shaderType));

	return shader;
}

void ShadersManager::checkCompileErrors(const GLuint &object, std::string type) {
	GLint success;
	GLchar infoLog[1024];

	if (type != "Program") {
		glGetShaderiv(object, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << std::endl;
			std::cout << infoLog << "\n -- --------------------------------------------------- -- \n";
		}
	}
	else {
		glGetProgramiv(object, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::Shader: Link-time error: Type: " << type << std::endl;
			std::cout << infoLog << "\n -- --------------------------------------------------- -- \n";
		}
	}
}

std::string ShadersManager::shaderTypeToString(GLenum shaderType) {
	std::string ret = "";

	switch (shaderType) {
	case GL_COMPUTE_SHADER:
		ret = "Compute";
		break;
	case GL_VERTEX_SHADER:
		ret = "Vertex";
		break;
	case GL_TESS_CONTROL_SHADER:
		ret = "Tessellation Control";
		break;
	case GL_TESS_EVALUATION_SHADER:
		ret = "Tessellation Evaluation";
		break;
	case GL_GEOMETRY_SHADER:
		ret = "Geometry";
		break;
	case GL_FRAGMENT_SHADER:
		ret = "Fragment";
		break;
	default:
		ret = "unknown";
		break;
	}
	return ret;
}


const GLuint ShadersManager::GetShader(const std::string& shaderName)
{

	return programs.at(shaderName);

}