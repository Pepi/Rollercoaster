#pragma once
#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace MiniEngine
{
	namespace Core
	{
		namespace Init
		{
			class GlewCore
			{
			public:
				GlewCore() {};
				~GlewCore() {};

				static void Init();
			};
		}
	}
}