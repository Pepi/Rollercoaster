#pragma once
#include "GenericObject.h"
#include "Camera.h"
#include "Fps.h"

#define G_FORCE		9.81f
#define FRICTION_VV	0.001f
#define FRICTION_V	0.03f
#define FRICTION_C	0.03f

namespace MiniEngine {
	namespace Rendering {
		namespace Models {

			class Train :
				public GenericObject
			{
			public:
				Train();
				~Train();

				void SetTrack(glm::vec3 position, glm::vec3 *track, glm::vec3 *tan, glm::vec3 *up, glm::vec3 *lat, GLfloat *pull, GLuint64 len);
				bool Initialize();
				void SetCamera(Camera* camera);
				void Update() override final;

			protected:

			private:
				void Move(GLfloat dt);

				Camera *mCamera;

				glm::vec3 mTrackLocation;
				glm::vec3 *mTrack;
				glm::vec3 *mTanVec;
				glm::vec3 *mUpVec;
				glm::vec3 *mLatVec;
				GLfloat *mPullVec;

				GLfloat mSpeed;
				GLfloat dx;

				GLuint64 mTrackLength;
				GLuint64 mTrackPosition;
			};

		}
	}
}


