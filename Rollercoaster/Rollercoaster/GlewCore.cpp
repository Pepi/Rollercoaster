#include "GlewCore.h"
using namespace MiniEngine::Core::Init;

void GlewCore::Init()
{

	glewExperimental = true;

	if (glewInit() == GLEW_OK)
	{
		std::cout << "GLEW: Initialized" << std::endl;
	}
	int s = glGetError();
	if (glewIsSupported("GL_VERSION_3_3"))
	{
		std::cout << "GLEW GL_VERSION_3_3 is 3.3\n ";
	}
	else
	{
		std::cout << " GLEW GL_VERSION_3_3 not supported\n ";
	}
}

