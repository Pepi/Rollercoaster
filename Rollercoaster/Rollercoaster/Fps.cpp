///////////////////////////////////////////////////////////////////////////////
// Filename: Fps.cpp
///////////////////////////////////////////////////////////////////////////////
#include "Fps.h"
#include <iostream>

using namespace MiniEngine::Core;

int Fps::m_fps;
int Fps::m_count;
float Fps::m_dt;
unsigned long Fps::m_startTime;

Fps::Fps()
{
}


Fps::Fps(const Fps& other)
{
}


Fps::~Fps()
{
}


void Fps::Initialize()
{
	m_fps = 0;
	m_count = 0;
	m_dt = 0.0f;
	m_startTime = timeGetTime();
	return;
}


void Fps::Frame()
{
	m_count++;

	if (timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_dt = 1.0f / m_fps;
		m_count = 0;

		m_startTime = timeGetTime();
		std::cout << m_fps << " FPS" << std::endl;
	}
}


int Fps::GetFps()
{
	return m_fps;
}

float Fps::GetDt() {
	return m_dt;
}