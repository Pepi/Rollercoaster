#pragma once
#include <glm\glm.hpp>
namespace MiniEngine
{
	namespace Rendering
	{
		struct VertexFormat
		{

			glm::vec3 position;
			glm::vec4 color;
			glm::vec2 texture;
			glm::vec3 normal;
			glm::vec3 tangent;
			glm::vec3 binormal;

			VertexFormat(const glm::vec3 &iPos, const glm::vec4 &iColor)
			{
				position = iPos;
				color = iColor;
				texture.s = texture.t = 0.0f;
				normal.x = normal.y = normal.z = 0.0f;
				tangent.x = tangent.y = tangent.z = 0.0f;
				binormal.x = binormal.y = binormal.z = 0.0f;
			}
			VertexFormat(const glm::vec3 &iPos, const glm::vec2 &iTexture)
			{
				position = iPos;
				texture = iTexture;
				color.r = color.g = color.b = color.a = 0;
				normal.x = normal.y = normal.z = 0;
				tangent.x = tangent.y = tangent.z = 0.0f;
				binormal.x = binormal.y = binormal.z = 0.0f;
			}
			VertexFormat(const glm::vec3 &iPos, const glm::vec2 &iTexture,const glm::vec4 &iColor, const glm::vec3 &iNormal)
			{
				position = iPos;
				texture = iTexture;
				color = iColor;
				normal = iNormal;
				tangent.x = tangent.y = tangent.z = 0.0f;
				binormal.x = binormal.y = binormal.z = 0.0f;
			}

			VertexFormat()
			{

			}
		};
	}
}
