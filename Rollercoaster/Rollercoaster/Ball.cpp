#include "Ball.h"

using namespace MiniEngine::Rendering::Models;

Ball::Ball()
{
	this->mPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	this->mRadius = 1.0f;
	this->mModelMatrix = glm::translate(glm::mat4(1.0f), this->mPosition);
	this->mBody = NULL;
}

Ball::Ball(float rad, glm::vec3 position) {
	this->mRadius = rad;
	this->mPosition = position;
	this->mModelMatrix = glm::translate(glm::mat4(1.0f), this->mPosition);
	this->mBody = NULL;
}

Ball::~Ball()
{
}

bool Ball::Initialize(GLchar *objFile) {

	bool error;

	this->mHasColorTex = false;
	this->mHasNormalTex = false;
	this->mHasGlossTex = false;
	this->mHasTessellation = true;

	/* Load Obj File */
	error = LoadObjfile(objFile);
	if (error) {
		std::cout << "Failed to load Object" << std::endl;
		return EXIT_FAILURE;
	}

	/* Set Color */
	for (long i = 0; i < mVertexSize; i++) {
		this->mVertex[i].color = glm::vec4(sin(0.01f*i), 0.7f, cos(0.03f*i), 1.0f);
	}

	this->Create();


	btTransform t;
	t.setIdentity();
	t.setOrigin(btVector3(this->mPosition.x, this->mPosition.y, this->mPosition.z));
	btSphereShape* sphere = new btSphereShape(1.0f);
	btVector3 inertia(0, 0, 0);
	sphere->calculateLocalInertia(1.0f, inertia);
	btMotionState* smotion = new btDefaultMotionState(t);
	mBody = new btRigidBody(1.0f, smotion, sphere, inertia);

	return EXIT_SUCCESS;
}

void Ball::Update() {
	if (mBody) {

		btTransform t;
		mBody->getMotionState()->getWorldTransform(t);
		float mat[16];
		t.getOpenGLMatrix(mat);

		float x, y, z, w;
		w = mat[15];
		x = mat[12] / w;
		y = mat[13] / w;
		z = mat[14] / w;

		//this->mModelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
		this->mModelMatrix = glm::make_mat4( mat );

		std::cout << "New position: " << x << "," << y << "," << z << std::endl;
	}
}


btRigidBody *Ball::GetBody() {
	return mBody;
}