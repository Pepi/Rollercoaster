#include "SkyBox.h"
#include <SOIL.h>

using namespace MiniEngine::Rendering::Models;

SkyBox::SkyBox()
{
}


SkyBox::~SkyBox()
{
}

void SkyBox::Create() {
	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLfloat size = 2000.0f;

	GLfloat vertices[] = {
		-size, size, -size,
		-size, -size, -size,
		size, -size, -size,
		size, -size, -size,
		size, size, -size,
		-size, size, -size,
		-size, -size, size,
		-size, -size, -size,
		-size, size, -size,
		-size, size, -size,
		-size, size, size,
		-size, -size, size,
		size, -size, -size,
		size, -size, size,
		size, size, size,
		size, size, size,
		size, size, -size,
		size, -size, -size,
		-size, -size, size,
		-size, size, size,
		size, size, size,
		size, size, size,
		size, -size, size,
		-size, -size, size,
		-size, size, -size,
		size, size, -size,
		size, size, size,
		size, size, size,
		-size, size, size,
		-size, size, -size,
		-size, -size, -size,
		-size, -size, size,
		size, -size, -size,
		size, -size, -size,
		-size, -size, size,
		size, -size, size
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
	glBindVertexArray(0);
	this->vao = vao;
	this->vbos.push_back(vbo);
}

void SkyBox::loadTexture(GLchar* filename) {
	std::vector<std::string> textures = {
		"./Images/SkyBoxes/Maskonaive/posx.jpg",
		"./Images/SkyBoxes/Maskonaive/negx.jpg",
		"./Images/SkyBoxes/Maskonaive/posy.jpg",
		"./Images/SkyBoxes/Maskonaive/negy.jpg",
		"./Images/SkyBoxes/Maskonaive/posz.jpg",
		"./Images/SkyBoxes/Maskonaive/negz.jpg"
	};

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	int iWidth, iHeight, iChannels;
	unsigned char* image;
	for (GLuint i = 0; i < textures.size(); i++)
	{
		image = SOIL_load_image(textures[i].c_str(), &iWidth, &iHeight, &iChannels, false);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, iWidth, iHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		SOIL_free_image_data(image);
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void SkyBox::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) {
	// Remove depth writing
	glDepthMask(GL_FALSE);

	glUseProgram(program);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture);
	glUniform1i(glGetUniformLocation(program, "skybox"), 0);
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, false, &projection_matrix[0][0]);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	// Re enable depth writing

	glDepthMask(GL_TRUE);
}

void SkyBox::Update() {

}
