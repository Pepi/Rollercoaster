#pragma once
#include <GL\glew.h>
#include <fstream>
#include <iostream>
#include <string>
#include "BMPHeaders.h"
#include "SOIL.h"

namespace MiniEngine
{
	namespace Rendering
	{
		class TextureLoader
		{
		public:
			TextureLoader();
			~TextureLoader();

			unsigned int LoadTexture(GLchar *filename);

		private:

			void LoadBMPFile(const std::string& filename,
				int& width,
				int& height,
				unsigned char*& data);
		};



	}
}