#pragma once
#include "GenericObject.h"
#include "Train.h"
namespace MiniEngine {
	namespace Rendering {
		namespace Models {
			class Rollercoaster : public GenericObject
			{
			public:
				Rollercoaster();
				~Rollercoaster();

				bool Initialize(GLchar *objFile, GLchar *tracFile);
				void SetTrain(Train *train);
				Train *GetTrain();
				void Update() override final;

			protected:

			private:
				bool LoadTrack(GLchar* tracFile);

				glm::vec3 mPosition;

				GLuint64 mTrackLength;

				glm::vec3 *mTrack;
				glm::vec3 *mTanVec;
				glm::vec3 *mUpVec;
				glm::vec3 *mLatVec;
				GLfloat *mPullVec;

				Train *mTrain;
			};
		}
	}
}


