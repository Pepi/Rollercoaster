#pragma once
#include "Model.h"
#include "ObjLoader.h"

namespace MiniEngine {
	namespace Rendering {
		namespace Models {
			class GenericObject :
				public Model
			{
			public:
				GenericObject();
				~GenericObject();

				bool LoadObjfile(GLchar* file);
				void Create();
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) override;
				virtual void Update() override;

			protected:

				VertexFormat *mVertex;
				GLuint64 mVertexSize;
				glm::mat4 mModelMatrix;

				bool mHasColorTex;
				bool mHasNormalTex;
				bool mHasDisplacementTex;
				bool mHasGlossTex;
				bool mHasOcclusionTex;
				bool mHasTessellation;
			};
		}
	}
}