#pragma once
#include "GlfwCore.h"
#include "SceneManager.h"
#include "CollisionManager.h"

#include <stdio.h>
#include <stdlib.h>

namespace MiniEngine
{
	class Engine
	{
	public:
		Engine();
		~Engine();
	public:
		bool Init();
		void Run();

		Managers::SceneManager*  GetScene_Manager()  const;
		Managers::ShadersManager* GetShader_Manager() const;
		Managers::ModelsManager* GetModels_Manager() const;
		Managers::CollisionManager* GetCollision_Manager() const;

		TextureLoader* GetTexture_Loader() const;

	private:
		Managers::SceneManager*  m_scene_manager;
		Managers::CollisionManager* m_collision_manager;
		Managers::ShadersManager* m_shader_manager;
		Managers::ModelsManager* m_models_manager;

		TextureLoader* m_texture_loader;
	};
}
