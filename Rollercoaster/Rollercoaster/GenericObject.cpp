#include "GenericObject.h"

using namespace MiniEngine::Rendering::Models;

GenericObject::GenericObject()
{
	this->mVertexSize = 0;
	this->mModelMatrix = glm::mat4(1.0f);
	this->mHasColorTex = false;
	this->mHasNormalTex = false;
	this->mHasGlossTex = false;
	this->mHasTessellation = false;
}

GenericObject::~GenericObject()
{
	free(this->mVertex);
	this->mVertexSize = 0;
}

bool GenericObject::LoadObjfile(GLchar* file) {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> textures;
	std::vector<glm::vec4> colors;
	std::vector<glm::vec3> normals;
	
	
	if (!ObjLoader::loadObj(file, vertices, textures, normals, colors))
		return EXIT_FAILURE;

	free(this->mVertex);
	this->mVertexSize = vertices.size();
	this->mVertex = new VertexFormat[this->mVertexSize];

	for (long i(0); i < mVertexSize; i++) {
		VertexFormat vert = VertexFormat(vertices[i], textures[i], colors[i], normals[i]);
		this->mVertex[i] = vert;
	}
	std::cout << "Object loaded" << std::endl;

	return EXIT_SUCCESS;
}

void GenericObject::Create() {
	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, this->mVertexSize * sizeof(VertexFormat), mVertex, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::position)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::normal)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::color)));

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::texture)));

	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::tangent)));

	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (GLvoid*)(offsetof(VertexFormat, VertexFormat::binormal)));

	glBindVertexArray(0);

	this->vao = vao;
	this->vaoSize = mVertexSize;
	this->vbos.push_back(vbo);

	free(this->mVertex);
	this->mVertex = 0;
	this->mVertexSize = 0;
}

void GenericObject::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) {

	glBindVertexArray(this->GetVao());
	glUseProgram(this->program);
	
	if (this->mHasColorTex) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->GetTexture("ColorTexture"));
		glUniform1i(glGetUniformLocation(this->program, "colorTex"), 0);
	}
	if (this->mHasNormalTex) {
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, this->GetTexture("NormalTexture"));
		glUniform1i(glGetUniformLocation(this->program, "normalTex"), 1);
	}
	if (this->mHasGlossTex) {
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, this->GetTexture("GlossinessTexture"));
		glUniform1i(glGetUniformLocation(this->program, "glossinessTex"), 2);
	}

	glm::mat4 NormalMatrix = glm::transpose(glm::inverse(this->mModelMatrix));
	glUniform4f(glGetUniformLocation(this->program, "m_lightPos"), -100.0f, 400.0f, -100.0f, 1.0f);
	glUniform4f(glGetUniformLocation(this->program, "m_cameraPos"), camera_position.x, camera_position.y, camera_position.z, 1.0f);
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_model"), 1, GL_FALSE, glm::value_ptr(this->mModelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_view"), 1, GL_FALSE, glm::value_ptr(view_matrix));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_viewInv"), 1, GL_FALSE, glm::value_ptr(glm::inverse(view_matrix)));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_modelView"), 1, GL_FALSE, glm::value_ptr(view_matrix*this->mModelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_projection"), 1, GL_FALSE, glm::value_ptr(projection_matrix));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_pvm"), 1, GL_FALSE, glm::value_ptr(projection_matrix*view_matrix*this->mModelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(this->program, "m_normal"), 1, GL_FALSE, glm::value_ptr(NormalMatrix));

	if (this->mHasTessellation) {
		glPatchParameteri(GL_PATCH_VERTICES, 3);
		glDrawArrays(GL_PATCHES, 0, this->vaoSize);
	}
	else {
		glDrawArrays(GL_TRIANGLES, 0, this->vaoSize);
	}
	glBindVertexArray(0);
}

void GenericObject::Update() {

}