#pragma once
#include <vector>
#include <glm\glm.hpp>
#include <stdio.h>
#include <stdlib.h>

namespace MiniEngine {
	namespace Rendering {
		namespace Models {
			class ObjLoader {
			public:
				static bool loadObj(const char *path,
					std::vector<glm::vec3> & out_vertices,
					std::vector<glm::vec2> & out_uvs,
					std::vector<glm::vec3> & out_normals,
					std::vector<glm::vec4> & out_colors) {
					std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
					std::vector< glm::vec3 > temp_vertices;
					std::vector< glm::vec2 > temp_uvs;
					std::vector< glm::vec3 > temp_normals;

					bool hasNormals = false, hasTexCoords = false;

					FILE * file = 0;
					int error = fopen_s(&file, path, "r");
					if (file == NULL || error) {
						printf("Impossible to open the file !\n");
						return false;
					}

					glm::vec4 color = glm::vec4(0.3f, 0.3f, 0.3f, 1.0f);

					while (1) {

						char lineHeader[128];
						// read the first word of the line
						int res = fscanf_s(file, "%s", lineHeader, _countof(lineHeader));
						if (res == EOF)
							break; // EOF = End Of File. Quit the loop.

						if (strcmp(lineHeader, "v") == 0) {
							glm::vec3 vertex;
							fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
							temp_vertices.push_back(vertex);
						}
						else if (strcmp(lineHeader, "vt") == 0) {
							glm::vec2 uv;
							fscanf_s(file, "%f %f\n", &uv.x, &uv.y);
							temp_uvs.push_back(uv);
						}
						else if (strcmp(lineHeader, "vn") == 0) {
							glm::vec3 normal;
							fscanf_s(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
							temp_normals.push_back(normal);
						}
						else if (strcmp(lineHeader, "f") == 0) {
							if ((temp_uvs.size() > 0) && (temp_normals.size() > 0)) {
								unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
								int matches = fscanf_s(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
								if (matches != 9) {
									printf("File can't be read by our simple parser : ( Try exporting with other options)\n");
									return false;
								}
								vertexIndices.push_back(vertexIndex[0]);
								vertexIndices.push_back(vertexIndex[1]);
								vertexIndices.push_back(vertexIndex[2]);
								uvIndices.push_back(uvIndex[0]);
								uvIndices.push_back(uvIndex[1]);
								uvIndices.push_back(uvIndex[2]);
								normalIndices.push_back(normalIndex[0]);
								normalIndices.push_back(normalIndex[1]);
								normalIndices.push_back(normalIndex[2]);

								hasNormals = true;
								hasTexCoords = true;

							}
							else if (temp_uvs.size() > 0) {
								unsigned int vertexIndex[3], uvIndex[3];
								int matches = fscanf_s(file, "%d/%d %d/%d %d/%d\n", &vertexIndex[0], &uvIndex[0], &vertexIndex[1], &uvIndex[1], &vertexIndex[2], &uvIndex[2]);
								if (matches != 6) {
									printf("File can't be read by our simple parser : ( Try exporting with other options\n");
									return false;
								}
								vertexIndices.push_back(vertexIndex[0]);
								vertexIndices.push_back(vertexIndex[1]);
								vertexIndices.push_back(vertexIndex[2]);
								uvIndices.push_back(uvIndex[0]);
								uvIndices.push_back(uvIndex[1]);
								uvIndices.push_back(uvIndex[2]);

								hasTexCoords = true;
							}
							else if (temp_normals.size() > 0){
								unsigned int vertexIndex[3], normalIndex[3];
								int matches = fscanf_s(file, "%d//%d %d//%d %d//%d\n", &vertexIndex[0], &normalIndex[0], &vertexIndex[1], &normalIndex[1], &vertexIndex[2], &normalIndex[2]);
								if (matches != 6) {
									printf("File can't be read by our simple parser : ( Try exporting with other options\n");
									return false;
								}
								vertexIndices.push_back(vertexIndex[0]);
								vertexIndices.push_back(vertexIndex[1]);
								vertexIndices.push_back(vertexIndex[2]);
								normalIndices.push_back(normalIndex[0]);
								normalIndices.push_back(normalIndex[1]);
								normalIndices.push_back(normalIndex[2]);

								hasNormals = true;
							}
							else {
								unsigned int vertexIndex[3], normalIndex[3];
								int matches = fscanf_s(file, "%d %d %d\n", &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);
								if (matches != 3) {
									printf("File can't be read by our simple parser : ( Try exporting with other options\n");
									return false;
								}
								vertexIndices.push_back(vertexIndex[0]);
								vertexIndices.push_back(vertexIndex[1]);
								vertexIndices.push_back(vertexIndex[2]);
							}
						}
					}
					int numclosed = _fcloseall();
					// For each vertex of each triangle
					for (unsigned int i = 0; i < vertexIndices.size(); i++) {
						unsigned int vertexIndex = vertexIndices[i];
						glm::vec3 vertex = temp_vertices[vertexIndex - 1];
						out_vertices.push_back(vertex);
						out_colors.push_back(color);

						if (hasTexCoords) {
							unsigned int uvIndex = uvIndices[i];
							glm::vec2 uv = temp_uvs[uvIndex - 1];
							out_uvs.push_back(uv);
						}
						else {
							out_uvs.push_back(glm::vec2(0.0f, 0.0f));
						}

						if (hasNormals) {
							unsigned int normalIndex = normalIndices[i];
							glm::vec3 normal = temp_normals[normalIndex - 1];
							out_normals.push_back(normal);
						} else {
							out_normals.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
						}
					}
				}
			};
		}
	}
}