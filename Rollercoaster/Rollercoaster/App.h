#pragma once
#include "Engine.h"
#include "GenericObject.h"
#include "Rollercoaster.h"
#include "Terrain.h"
#include "SkyDome.h"
#include "Ball.h"

class App
{
public:
	App();
	~App();

	bool Initialize();
	bool Load();
	void Launch();

private:
	MiniEngine::Engine *engine;
};

