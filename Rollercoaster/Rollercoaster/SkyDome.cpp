#include "SkyDome.h"

using namespace MiniEngine::Rendering::Models;

SkyDome::SkyDome()
{
	mModelMatrix = glm::mat4(1.0f);
}


SkyDome::~SkyDome()
{
}

bool SkyDome::Initialize() {
	bool error;
	
	error = LoadObjfile("Objects/skydome.obj");
	if (error) {
		std::cout << "Unable to load skyDome obj file" << std::endl;
		return EXIT_FAILURE;
	}

	Create();

	return EXIT_SUCCESS;
}

void SkyDome::ListenToPosition(glm::vec3 *position) {
	mPosition = position;
}

void SkyDome::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) {
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(this->GetVao());
	glUseProgram(this->program);
	glUniform4f(glGetUniformLocation(program, "m_UpColor"), 0.1f, 0.1f, 0.7f, 1.0f);
	glUniform4f(glGetUniformLocation(program, "m_DownColor"), 0.4f, 0.4f, 1.0f, 1.0f);
	glUniform4f(glGetUniformLocation(program, "m_lightPos"), -100.0f, 400.0f, -100.0f, 1.0f);
	glUniform4f(glGetUniformLocation(program, "m_cameraPos"), camera_position.x, camera_position.y, camera_position.z, 1.0f);
	glUniformMatrix4fv(glGetUniformLocation(program, "m_model"), 1, GL_FALSE, glm::value_ptr(mModelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(program, "m_pvm"), 1, GL_FALSE, glm::value_ptr(projection_matrix*view_matrix*mModelMatrix));
	glDrawArrays(GL_TRIANGLES, 0, this->vaoSize);
	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void SkyDome::Update() {
	mModelMatrix = glm::translate(glm::mat4(1.0f), *mPosition);
}
