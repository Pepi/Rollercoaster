#pragma once
#include "GenericObject.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace MiniEngine
{
	namespace Rendering 
	{
		namespace Models 
		{
			class Ball :
				public GenericObject
			{
			public:
				Ball();
				Ball(float rad, glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f));
				~Ball();

				bool Initialize(GLchar *objFile);
				void Update() override final;

				btRigidBody *GetBody();

			private:
				glm::vec3 mPosition;
				float mRadius;

				btRigidBody* mBody;
			};
		}
	}
}
