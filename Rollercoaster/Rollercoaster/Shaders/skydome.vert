#version 430 core

layout (location = 0) in vec3 vertex;

uniform mat4 m_model;
uniform mat4 m_pvm;

out float vs_height;
out vec4 vs_position;

void main(){
	vs_height = vertex.y;
	vs_position = m_model * vec4(vertex,1.0f);
	gl_Position = m_pvm * vec4(vertex, 1.0f);
}