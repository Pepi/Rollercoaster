#version 430 core

uniform vec4 m_UpColor;
uniform vec4 m_DownColor;
uniform vec4 m_lightPos;
uniform vec4 m_cameraPos;

in float vs_height;
in vec4 vs_position;

out vec4 outColor;

void main(){
	
	float a = clamp(vs_height/2.0f, 0.0f, 1.0f);
	vec4 skyColor = mix(m_DownColor, m_UpColor, a);

	vec3 lightDir = normalize( (m_lightPos - vs_position).xyz );
	vec3 eyeDir = normalize( (vs_position - m_cameraPos).xyz );
	float b = clamp( dot( lightDir, eyeDir), 0.0f, 1.0f);
	vec4 sunLight = clamp( 1.2f * pow(b,12.0f), 0.0f, 1.0f) * vec4(1.0f,1.0f,1.0f,0.0f);

	outColor = skyColor + sunLight;
}
