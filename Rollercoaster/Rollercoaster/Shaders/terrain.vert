#version 430 core
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 texCoord;
layout (location = 4) in vec3 tangent;
layout (location = 5) in vec3 binormal;

uniform mat4 m_model;
uniform mat4 m_normal;

out vec3 WorldPos_CS_in;
out vec3 Color_CS_in;
out vec2 TexCoord_CS_in;
out vec3 Normal_CS_in;
out vec3 Tangent_CS_in;
out vec3 Binormal_CS_in;

void main()
{
	vec4 wVertexPos = m_model * vec4(vertex, 1.0f);

	vec3 wNormal = (m_normal * vec4(normal, 1.0f)).xyz;
	vec3 wTangent = (m_normal * vec4(tangent, 1.0f)).xyz;
	vec3 wBinormal = (m_normal * vec4(binormal, 1.0f)).xyz;

    WorldPos_CS_in = wVertexPos.xyz;
	Color_CS_in = color.rgb;
    TexCoord_CS_in = texCoord;
    Normal_CS_in = normalize( wNormal );
    Tangent_CS_in = normalize( wTangent );
    Binormal_CS_in = normalize( wBinormal );
}