#pragma once
#include <vector>
#include "IListener.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace MiniEngine
{
	namespace Managers
	{
		class CollisionManager : public Core::IListener
		{
		public:
			CollisionManager();
			~CollisionManager();

			virtual void NotifyBeginFrame();
			virtual void NotifyDisplayFrame();
			virtual void NotifyEndFrame();
			virtual void NotifyReshape(int width, int height, int previos_width, int previous_height);
			virtual void NotifyInput(bool *key, double xpos, double ypos, double dt);

			void AddRigidBody(btRigidBody *body);

		private:
			btDynamicsWorld* mWorld;
			btDispatcher* mDispatcher;
			btCollisionConfiguration* mCollisionConfig;
			btBroadphaseInterface* mBroadPhase;
			btConstraintSolver* mSolver;

			std::vector<btRigidBody*> mBodies;
		};
	}
}
