#include "Train.h"

using namespace MiniEngine::Rendering::Models;

Train::Train()
{
}


Train::~Train()
{
}

bool Train::Initialize() {

	bool error;

	this->mHasColorTex = false;
	this->mHasNormalTex = false;
	this->mHasGlossTex = false;
	this->mHasTessellation = true;

	error = LoadObjfile("Objects/sphere.obj");
	if (error) {
		std::cout << "Failed to load Object" << std::endl;
		return EXIT_FAILURE;
	}

	for (long i = 0; i < mVertexSize; i++) {
		this->mVertex[i].color = glm::vec4(0.6f, 0.3f, 0.5f, 1.0f);
	}

	this->Create();

	this->mTrackPosition = 0;
	this->mSpeed = 15.0f;
	this->dx = 0.0f;

	return EXIT_SUCCESS;
}

void Train::SetTrack(glm::vec3 position, glm::vec3 *track, glm::vec3 *tan, glm::vec3 *up, glm::vec3 *lat, GLfloat *pull, GLuint64 len) {

	this->mTrackLocation = position;
	this->mTrack = track;
	this->mTanVec = tan;
	this->mUpVec = up;
	this->mLatVec = lat;
	this->mPullVec = pull;
	this->mTrackLength = len;
}

void Train::SetCamera(Camera *camera) {
	this->mCamera = camera;
	this->mCamera->Locked = true;
}

void Train::Move(GLfloat dt) {

	this->dx += this->mSpeed * dt;

	GLfloat distance = 1.0f;
	GLfloat Alpha = 1.0f;

	if (mSpeed >= 0.0f) {
		while (dx >= 0.0f) {
			GLuint64 nextPos = this->mTrackPosition + 1;
			if (nextPos >= this->mTrackLength)
				nextPos = 0;

			distance = glm::distance(this->mTrack[this->mTrackPosition], this->mTrack[nextPos]);
			this->dx -= distance;

			this->mTrackPosition++;
			if (this->mTrackPosition >= this->mTrackLength)
				this->mTrackPosition = 0;
		}
	}
	else {
		while (dx <= 0.0f) {
			GLuint64 nextPos = this->mTrackPosition -1;
			if (nextPos > this->mTrackLength)
				nextPos = this->mTrackLength - 1;

			distance = glm::distance(this->mTrack[this->mTrackPosition], this->mTrack[nextPos]);
			this->dx += distance;

			this->mTrackPosition--;
			if (this->mTrackPosition > this->mTrackLength)
				this->mTrackPosition = this->mTrackLength - 1;
		}
	}
}

void Train::Update() {
	GLfloat dt = 1.0f / MiniEngine::Core::Fps::GetFps();
	if (dt > 0.05f)
		dt = 0.001f;

	GLuint64 nextPos;
	GLfloat alpha = 1.0f;

	if (mSpeed > 0.0f) {
		nextPos = this->mTrackPosition + 1;
		if (nextPos >= this->mTrackLength)
			nextPos = 0;

		GLfloat distance = glm::distance(this->mTrack[this->mTrackPosition], this->mTrack[nextPos]);
		alpha = dx / distance;
	}
	else
	{
		nextPos = this->mTrackPosition - 1;
		if (nextPos > this->mTrackLength)
			nextPos = this->mTrackLength - 1;

		GLfloat distance = glm::distance(this->mTrack[this->mTrackPosition], this->mTrack[nextPos]);
		alpha = 1.0 - dx / distance;
	}


	glm::vec3 tan = glm::mix(mTanVec[this->mTrackPosition], mTanVec[nextPos], alpha);
	glm::vec3 lat = glm::mix(mLatVec[this->mTrackPosition], mLatVec[nextPos], alpha);
	glm::vec3 up = glm::cross(tan, lat);

	GLfloat dv = G_FORCE * glm::dot(glm::vec3(0, -1, 0), tan);
	dv -= FRICTION_VV * this->mSpeed * glm::abs(this->mSpeed);
	dv -= FRICTION_V * this->mSpeed;
	dv -= FRICTION_C * glm::sign(this->mSpeed);
	this->mSpeed += dv*dt;
	if ((this->mPullVec[this->mTrackPosition] > 0.0f) && (this->mSpeed < this->mPullVec[this->mTrackPosition]))
		this->mSpeed = this->mPullVec[mTrackPosition];
	this->Move(dt);

	glm::vec3 position = mTrackLocation + mTrack[this->mTrackPosition] + dx * tan + 2.0f * up;
	this->mModelMatrix = glm::translate(glm::mat4(1.0f), position);

	if (this->mCamera->Locked) {
		this->mCamera->Position = position;
		this->mCamera->Front = tan;
		this->mCamera->Up = up;
	}
}