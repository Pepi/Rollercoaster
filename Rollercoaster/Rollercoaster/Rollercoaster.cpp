#include "Rollercoaster.h"

using namespace MiniEngine::Rendering::Models;

Rollercoaster::Rollercoaster()
{
	this->mModelMatrix = glm::mat4(1.0f);
	this->mPosition = glm::vec3(115.0f, 8.0f, 115.0f);
	this->mTrain = 0;
}


Rollercoaster::~Rollercoaster()
{
	delete this->mTrack;
	this->mTrack = 0;

	delete this->mTanVec;
	this->mTanVec = 0;
	
	delete this->mUpVec;
	this->mUpVec = 0;

	delete this->mLatVec;
	this->mLatVec = 0;

	delete this->mPullVec;
	this->mPullVec = 0;
}

bool Rollercoaster::Initialize(GLchar *objFile, GLchar* tracFile) {
	
	bool error;

	this->mHasColorTex = false;
	this->mHasNormalTex = false;
	this->mHasGlossTex = false;
	this->mHasTessellation = true;
	
	error = LoadObjfile(objFile);
	if (error) {
		std::cout << "Failed to load Object" << std::endl;
		return EXIT_FAILURE;
	}

	for (long i = 0; i < mVertexSize; i++) {
		this->mVertex[i].color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	this->Create();

	LoadTrack(tracFile);

	this->mModelMatrix = glm::translate(glm::mat4(1.0f), this->mPosition);

	return EXIT_SUCCESS;
}

bool Rollercoaster::LoadTrack(GLchar *tracFile) {
	FILE * file = 0;
	int error = fopen_s(&file, tracFile, "r");
	if (file == NULL || error) {
		printf("Impossible to open the file !\n");
		return false;
	}

	std::vector<glm::vec3> points;
	std::vector<glm::vec3> upVecs;
	std::vector<glm::vec3> tanVecs;
	std::vector<glm::vec3> latVecs;
	std::vector<GLfloat> pullVals;

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf_s(file, "%s", lineHeader, _countof(lineHeader));
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		else if (strcmp(lineHeader, "p") == 0) {
			glm::vec3 point;
			fscanf_s(file, "%f %f %f\n", &point.x, &point.y, &point.z);
			points.push_back(point);
		}else if (strcmp(lineHeader, "t") == 0) {
			glm::vec3 tanVec;
			fscanf_s(file, "%f %f %f\n", &tanVec.x, &tanVec.y, &tanVec.z);
			tanVecs.push_back(tanVec);
		}else if (strcmp(lineHeader, "u") == 0) {
			glm::vec3 upVec;
			fscanf_s(file, "%f %f %f\n", &upVec.x, &upVec.y, &upVec.z);
			upVecs.push_back(upVec);
		}else if (strcmp(lineHeader, "b") == 0) {
			glm::vec3 latVec;
			fscanf_s(file, "%f %f %f\n", &latVec.x, &latVec.y, &latVec.z);
			latVecs.push_back(latVec);
		}else if (strcmp(lineHeader, "a") == 0) {
			GLfloat pullVal;
			fscanf_s(file, "%f\n", &pullVal);
			pullVals.push_back(pullVal);
		}
	}

	this->mTrackLength = points.size();
	this->mTrack = new glm::vec3[mTrackLength];
	this->mTanVec = new glm::vec3[mTrackLength];
	this->mUpVec = new glm::vec3[mTrackLength];
	this->mLatVec = new glm::vec3[mTrackLength];
	this->mPullVec = new GLfloat[mTrackLength];

	for (GLuint64 i = 0; i < mTrackLength; i++) {
		this->mTrack[i] = points[i];
		this->mTanVec[i] = tanVecs[i];
		this->mUpVec[i] = upVecs[i];
		this->mLatVec[i] = latVecs[i];
		this->mPullVec[i] = pullVals[i];
	}

}

void Rollercoaster::SetTrain(Train *train) {
	this->mTrain = train;
	this->mTrain->SetTrack(mPosition, mTrack, mTanVec, mUpVec, mLatVec, mPullVec, mTrackLength);
}

Train *Rollercoaster::GetTrain() {
	return this->mTrain;
}

void Rollercoaster::Update() {
	if (this->mTrain != NULL) {
		this->mTrain->Update();
	}
}