#pragma once
#include "GenericObject.h"

namespace MiniEngine {
	namespace Rendering {
		namespace Models {
			class SkyDome :
				public GenericObject
			{
			public:
				SkyDome();
				~SkyDome();

			bool Initialize();
			void ListenToPosition(glm::vec3 *position);

			virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::vec3& camera_position) override final;
			void Update() override final;
			private:
				glm::vec3 *mPosition;
			};
		}
	}
}


