#include "CollisionManager.h"
#include "Fps.h"

using namespace MiniEngine;
using namespace Managers;


CollisionManager::CollisionManager()
{
	mCollisionConfig = new btDefaultCollisionConfiguration();
	mDispatcher = new btCollisionDispatcher(mCollisionConfig);
	mBroadPhase = new btDbvtBroadphase();
	mSolver = new btSequentialImpulseConstraintSolver();
	mWorld = new btDiscreteDynamicsWorld(mDispatcher, mBroadPhase, mSolver, mCollisionConfig);
	mWorld->setGravity(btVector3(0.0f, -9.81f, 0.0f));
}


CollisionManager::~CollisionManager()
{
	for (unsigned long i = 0; i < this->mBodies.size(); i++) {
		delete mBodies[i];
	}

	if(mDispatcher)
		delete mDispatcher;
	if(mCollisionConfig)
		delete mCollisionConfig;
	if(mSolver)
		delete mSolver;
	if(mWorld)
		delete mWorld;
	if (mBroadPhase)
		delete mBroadPhase;
}

void CollisionManager::NotifyBeginFrame() {
	mWorld->stepSimulation(MiniEngine::Core::Fps::GetDt());
}

void CollisionManager::NotifyDisplayFrame() {

}

void CollisionManager::NotifyEndFrame() {

}

void CollisionManager::NotifyReshape(int width, int height, int previos_width, int previous_height) {

}

void CollisionManager::NotifyInput(bool *key, double xpos, double ypos, double dt) {

}

void CollisionManager::AddRigidBody(btRigidBody *body) {
	this->mWorld->addRigidBody(body);
	this->mBodies.push_back(body);
}
