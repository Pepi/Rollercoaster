#version 430 core

in vec3 WorldPos_FS_in;
in vec3 Color_FS_in;
in vec2 TexCoord_FS_in;
in vec3 Normal_FS_in;
in vec3 Tangent_FS_in;
in vec3 Binormal_FS_in;

uniform mat4 m_normal;
uniform vec4 m_lightPos;
uniform vec4 m_cameraPos;

uniform sampler2D colorTex;
uniform sampler2D normalTex;
uniform sampler2D glossinessTex;
uniform sampler2D occlusionTex;

out vec4 fcolor;

void main()
{
    float ambiant = 0.1f;
	float shininess = 12.0f;
	float specular = 0.25f;
	float diffuse = 0.7f;

    vec3 textureColor = texture(colorTex, TexCoord_FS_in).xyz;
    vec4 mapNormal = texture(normalTex, TexCoord_FS_in);
    vec4 glossiness = texture(glossinessTex, TexCoord_FS_in);
    vec4 occlusion = texture(occlusionTex, TexCoord_FS_in);

    mapNormal = (mapNormal * 2.0f) - 1.0f;
    vec3 bumpNormal = mapNormal.x * Tangent_FS_in + mapNormal.y * Binormal_FS_in + mapNormal.z * Normal_FS_in;
    bumpNormal = normalize(bumpNormal);

    vec3 wEyeDir = normalize(m_cameraPos.xyz - WorldPos_FS_in);
    vec3 wLightDir = normalize(m_lightPos.xyz - WorldPos_FS_in);

    vec3 n = bumpNormal;
    vec3 l = wLightDir;
    vec3 r = -reflect( l, n );
    vec3 e = wEyeDir;

   	glossiness = normalize(glossiness);
    float dg = glossiness.x + glossiness.y + glossiness.z;

    float diff = clamp( dot( n,l ), 0,1 );
    float spec = clamp( dg * pow( max( 0.0f,dot ( e,r ) ),shininess ), 0,1 );

    textureColor = mix( textureColor, Color_FS_in , 0.8f );

    vec3 ambiantColor = ambiant * textureColor;
    vec3 diffColor = diffuse * diff * textureColor;
    vec3 specColor = specular * spec * vec3(1,1,1);

    fcolor =  vec4(clamp(ambiantColor + diffColor + specColor,0,1), 1.0f );
}