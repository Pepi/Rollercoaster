#version 410 core

layout (location = 2) in vec4 m_color;
uniform vec4 m_light;

in vec4 eyeNorm;
in vec4 eyeCord;
in vec3 vs_normal;

out vec4 color;

void main()
{
    vec4 s = normalize(m_light - eyeCord) ;
    vec4 r = reflect(-s,eyeNorm);
    vec4 v = normalize(-eyeCord);
    float spec = max( dot(v,r),0.0 );
    float diff = max(dot(eyeNorm,s),0.0);

    vec3 diffColor = 0.3 * diff * m_color.rbg;
    vec3 specColor = 0.2 * pow(spec,3) * vec3(1,1,1);
    vec3 ambientColor = 0.7 * m_color.rbg;
    color = vec4(normalize(vs_normal), 1.0f);

    color =  vec4(clamp(diffColor + specColor + ambientColor,0.0f, 1.0f), 1.0f);
}