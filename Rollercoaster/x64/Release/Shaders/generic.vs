#version 430 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 3) in vec2 texCoord;

uniform mat4 m_model;
uniform mat4 m_modelView;
uniform mat4 m_pvm;
uniform mat4 m_normal;

out vec3 WorldPos_CS_in;
out vec2 TexCoord_CS_in;
out vec3 Normal_CS_in;

void main()
{

	WorldPos_CS_in = (m_model * vec4(vertex, 1.0)).xyz;
    TexCoord_CS_in = texCoord;
    Normal_CS_in = normalize( (m_normal * vec4(normal, 1.0)).xyz);

}