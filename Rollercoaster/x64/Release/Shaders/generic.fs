#version 430 core

uniform vec4 m_lightPos;
uniform vec4 m_cameraPos;

in vec3 WorldPos_FS_in;
in vec2 TexCoord_FS_in;
in vec3 Normal_FS_in;

out vec4 fcolor;

void main()
{
	float shininess = 10.0f;

	vec3 color = vec3(0.7f, 0.2f, 0.2f);

	vec3 l = normalize( m_lightPos.xyz - WorldPos_FS_in );
	vec3 e = normalize ( m_cameraPos.xyz - WorldPos_FS_in );
	vec3 n = normalize( Normal_FS_in );
    vec3 r = -reflect( l,n );

	float diff = clamp(dot( l,n ), 0,1 );
    float spec = clamp( pow( max( 0.0f,dot ( e,r ) ),shininess ), 0,1 );

	vec3 diffColor = diff * color.rgb;
	vec3 specColor = spec * vec3(1,1,1);

    fcolor = vec4(clamp(0.2f*color + 0.8f * diffColor + 0.4f * specColor, 0,1), 1.0f);
}