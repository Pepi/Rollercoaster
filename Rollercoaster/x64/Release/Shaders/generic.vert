#version 410 core
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

uniform mat4 m_model;
uniform mat4 m_modelView;
uniform mat4 m_pvm;
uniform mat4 m_normal;

out vec4 eyeNorm;
out vec4 eyeCord;

out vec3 vs_normal;

void main()
{
    eyeNorm = normalize(m_normal * vec4(normal, 0.0));
    eyeCord = m_modelView * vec4(vertex, 1.0);

    vs_normal = normal;

    gl_Position = m_pvm * vec4(vertex, 1.0);
}